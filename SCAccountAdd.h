#pragma once

#define DaySec       (24*60*60)

typedef struct
   {
   iAgency  AgencyID ;
   DWORD    ProductType ;
   DWORD    ProductDesig ;
   DWORD    AccStatus ;
   DWORD    UserProfile ;
   DWORD    ExpDate ;
   DWORD    OperStatus ;
   iAgency  CardAgencyID ;
   char     CardUUID[32] ;
   BYTE     file[3][32] ;
   } CARD_PARAMS ;

extern CARD_PARAMS  CardParams ;
extern HWND hwndMain ;
extern HINSTANCE  hInst ;
extern HANDLE hLogFile ;

void ErrorBox(HWND hwnd, const char *fmt, ...) ;

void SetFileVersions(iAgency DES_Agency) ;

void    DESSetKeyData(KEY *pKey, size_t KeyLen) ;
BOOLEAN DESCryptDataBlock(KEY *pKey, unsigned char *Out, const unsigned char *In, DESFireCipher e_Cipher) ;
BOOLEAN AESCryptDataBlock(KEY *pKey, unsigned char *Out, const unsigned char *In, DESFireCipher e_Cipher) ;

// Files.c
BOOLEAN CreateFileNames(void) ;
DWORD   WriteOutFile(HANDLE hFile, char *fmt, ...) ;

// Operations
PROCESS_STATE  ProcessCard(HWND hWnd, CARD_PARAMS *lpParams) ;
PROCESS_STATE  CheckCard(HWND hWnd, CARD_PARAMS *lpParams) ;

char *FormatExpDate(DWORD d) ;
char *FormatDate(void) ;

