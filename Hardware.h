// Hardware interface

#ifndef _HARDWARE_H_
#define _HARDWARE_H_

#include <Windows.h>

typedef DWORD uint32_t ;
typedef WORD  uint16_t ;
typedef BYTE  uint8_t ;

typedef enum
   {
   CARD_STATE_SUCCESS=0,
   CARD_STATE_ALREADY,
   CARD_STATE_ERROR,
   CARD_STATE_NO,
   CARD_STATE_CREATE
   } PROCESS_STATE ;

typedef enum
   {
   CARD_STATE_UNKNOWN = 0,
   CARD_STATE_ABORT,
   CARD_STATE_EMPTY,
   CARD_STATE_PRESENT
   } CARD_STATE ;


typedef  char*   (*SCGETDEVLIST)(DWORD*) ;
typedef  LONG    (*SCFREEDEVLIST)(void) ;
typedef  LONG    (*SCINITDEV)(DWORD) ;
typedef  LONG    (*SCOPEN)(DWORD) ;
typedef  LONG    (*SCCLOSE)(void) ;
typedef  HANDLE  (*SCDETECT)(DWORD, HWND) ;
typedef  LONG    (*SCTRANSMIT)(const unsigned char*, size_t, unsigned char*, DWORD*) ;
typedef  char*   (*GETUUID)(void) ;
typedef  WORD    (*GETSW12)(void) ;
typedef  const char* (*SCGETLASTERROR)(void) ;
typedef  void    (*SETERROR)(LONG) ;
typedef  BYTE    (*GETCARDTYPE)(void) ;
typedef  LONG    (*ULC_AUTH)(BYTE, const unsigned char*) ;
typedef  LONG    (*DES_AUTH)(void*) ;
typedef  LONG    (*MIFARE_AUTH)(uint8_t) ;


typedef struct
   {
   DWORD          IS_PCSC ;
   SCGETDEVLIST   GetDevList ;
   SCFREEDEVLIST  FreeDevList ;
   SCOPEN         Open ;
   SCCLOSE        Close ;
   SCDETECT       IsCardPresent ;
   SCTRANSMIT     Push ;
   GETUUID        getUUID ;
   GETUUID        getATR ;
   GETSW12        getSW12 ;
   SETERROR       SetLastError ;
   SCGETLASTERROR GetLastSCError ;
   GETCARDTYPE    GetCardType ;
   ULC_AUTH       ULC_Authenticate ;
   DES_AUTH       DES_Authenticate ;
   MIFARE_AUTH    Mifare_Auth ;
   } HARDWARE ;


HARDWARE* GetPCSCIntrface(void) ;


typedef struct
   {
   DWORD  devNum ;
   HANDLE hEvent ;
   HWND   hwnd ;
   } DETECT_PARAMS ;

extern HARDWARE  *h ;

#endif