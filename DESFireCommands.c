#include "stdafx.h"

static Application LastSelectedApplication = UNKNOWN_APPLICATION ;
static DES_KEY     LastAuthenticationKey = NOT_AUTHENTICATED ;
static KEY         SessionKey ;
static BOOLEAN     commit = FALSE ;


LONG DES_getApplicationIDs(HARDWARE *h, VECTOR_DWORD *app_list)
   {
   static const unsigned char getApplicationIDsCmd[] = {0x90, 0x6A, 0x00, 0x00, 0x00} ;
   static const unsigned char FEIG_getApplicationIDsCmd[] = {0xC3, 0x6A, 0x00, 0x00} ;
	unsigned char cmd[64] ;
	DWORD  p, cmdLen = sizeof(cmd) ;
   LONG   rc ;

	/// <Comment> This method is not hacked to the end. The veriClass 3300 reader returns unspecified 
	/// data stream. Currently we do not know how to determin the end of data in the response.
	/// This can cause that remaining (probably uninitialized) data can be interpreted as Application ID.
	/// Several experiments showed that the device attaches randomly 8 bytes of data. See getApplicationIDs.docx document
	/// The method obtains the first 19 AIDs as a result of this
	/// </Comment>
   VD_Clear(app_list) ;
   rc = h->Push(getApplicationIDsCmd, (DWORD)sizeof(getApplicationIDsCmd), cmd, &cmdLen) ;
	if (rc) return rc ;
	for (p = 0; p < cmdLen; p += 3)
      {
		VD_Append(app_list, (DWORD)(cmd[p]) | ((DWORD)(cmd[p + 1]) << 8) | ((DWORD)(cmd[p + 2]) << 16)) ;
	   }
   return 0 ;
   }


LONG DES_selectApplication(HARDWARE *h, Application App)
   {
   static const unsigned char	SelectApplicationCmd[] = {0x90, 0x5A, 0x00, 0x00, 0x03, 0x00, 0x00, 0x00, 0x00} ;
   static const unsigned char	FEIG_SelectApplicationCmd[] = {0xC3, 0x5A, 0x00, 0x00} ;
	unsigned char cmd[16] ;
	DWORD cmdLen = (DWORD)sizeof(cmd) ;
   LONG  rc ;

   memcpy(cmd, SelectApplicationCmd, sizeof(SelectApplicationCmd)) ;
	cmd[5] = (unsigned char)App ;
	cmd[6] = (unsigned char)(App >> 8) ;
	cmd[7] = (unsigned char)(App >> 16) ;
   rc = h->Push(cmd, sizeof(SelectApplicationCmd), cmd, &cmdLen) ;
   if (rc)
      LastSelectedApplication = UNKNOWN_APPLICATION ;
   else
      LastSelectedApplication = App ;
   return rc ;
   }


iAgency DES_GetAgencyIDFromApp(HARDWARE *h, Application app)
   {
   unsigned char buffer[32] ;

   if (DES_selectApplication(h, app)) return 0 ;
   if (DES_readData(h, 0, 0, 32, buffer)) return 0 ;
   if (crc16(buffer, 32)) return 0 ;
   return ((WORD)(buffer[28]) << 8) | (WORD)(buffer[29]) ;
   }


LONG DES_readData(HARDWARE *h, BYTE FileNo, unsigned int Idx, unsigned int len, unsigned char *File)
   {
   static const unsigned char ReadDESCmd[] = {0x90, 0xBD, 0x00, 0x00, 0x07, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00} ;
   static const unsigned char FEIG_ReadDESCmd[] = {0xC3, 0xBD, 0x00, 0x00, 0x00, 0x00} ;
   static const unsigned char ContinueCmd[] = {0x90, 0xAF, 0x00, 0x00, 0x00} ;
   unsigned char cmd[128] ;
	DWORD  cmdLen, totalLen = 0 ;
   LONG   rc ;

   if (len == 0) return 0 ;

   memcpy(cmd, ReadDESCmd, (DWORD)sizeof(ReadDESCmd)) ;
   cmd[5]  = FileNo ;
   cmd[6]  = (unsigned char)Idx ;
   cmd[7]  = (unsigned char)(Idx >> 8) ;
   cmd[8]  = (unsigned char)(Idx >> 16) ;
   cmd[9]  = (unsigned char)len ;
   cmd[10] = (unsigned char)(len >> 8) ;
   cmd[11] = (unsigned char)(len >> 16) ;

   cmdLen = (DWORD)sizeof(cmd) ;
   if ((rc = h->Push(cmd, (DWORD)sizeof(ReadDESCmd), cmd, &cmdLen))) return rc ;
   do
      {
      if (cmdLen > 0)
         {
         if (cmdLen > len) cmdLen = len ;
         memcpy(File + totalLen, cmd, cmdLen) ;
         len -= cmdLen ;
         } ;
      if (len == 0 || h->getSW12() != ADDITIONAL_DATA_FRAME_TO_BE_SEND) return 0 ;
      totalLen += cmdLen ;
      cmdLen = (DWORD)sizeof(cmd) ;
      }
   while (0 == (rc = h->Push(ContinueCmd, (DWORD)sizeof(ContinueCmd), cmd, &cmdLen))) ;

	return rc ;
   }


LONG DES_readRecord(HARDWARE *h, BYTE FileNo, unsigned int RecNum, unsigned char *File, unsigned int len)
   {
   static const unsigned char ReadRecordCmd[] = {0x90, 0xBB, 0x00, 0x00, 0x07, 0x00, 0x00, 0x00, 0x00, 0x01, 0x00, 0x00, 0x00} ;
   static const unsigned char FEIG_ReadRecordCmd[] = {0xC3, 0xBB, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00} ;
   unsigned char cmd[128] ;
	DWORD  cmdLen = (DWORD)sizeof(cmd), totalLen = 0 ;
   LONG   rc ;

   memcpy(cmd, ReadRecordCmd, (DWORD)sizeof(ReadRecordCmd)) ;
   cmd[5]  = FileNo ;
   cmd[6]  = (unsigned char)RecNum ;
   cmd[7]  = (unsigned char)(RecNum >> 8) ;
   cmd[8]  = (unsigned char)(RecNum >> 16) ;
   if ((rc = h->Push(cmd, (DWORD)sizeof(ReadRecordCmd), cmd, &cmdLen))) return rc ;

   if (cmdLen > len) cmdLen = len ;
   if (cmdLen) memcpy(File, cmd, cmdLen) ;
	return rc ;
   }


LONG DES_getFileIDs(HARDWARE *h, VECTOR_BYTE *pFileIDs)
   {
   static const unsigned char getFileIDsCmd[] = {0x90, 0x6F, 0x00, 0x00, 0x00} ;
   static const unsigned char FEIG_getFileIDsCmd[] = {0xC3, 0x6F, 0x00, 0x00} ;
	unsigned char cmd[64] ;
	DWORD  cmdLen = (DWORD)sizeof(cmd) ;
   LONG    rc ;
   size_t  p ;

   pFileIDs->numElements = 0 ;
   rc = h->Push(getFileIDsCmd, (DWORD)sizeof(getFileIDsCmd), cmd, &cmdLen) ;
   if (rc) return rc ;
	for (p = 0; p < cmdLen; p++) VB_Append(pFileIDs, cmd[p]) ;
	return 0 ;
   }



BOOLEAN CryptDataBlock(KEY *pKey, unsigned char *Out, const unsigned char *In, DESFireCipher e_Cipher)
   {
   if (pKey->keyType == KEY_AES) return AESCryptDataBlock(pKey, Out, In, e_Cipher) ;
   return DESCryptDataBlock(pKey, Out, In, e_Cipher) ;
   }


BOOLEAN CryptDataCBC(KEY *pKey, DESFireCBC e_CBC, DESFireCipher e_Cipher, unsigned char* Out, const unsigned char* In, size_t ByteCount)
   {
	unsigned char Temp[16] ;
   size_t  n ;

   n = ByteCount/pKey->BlockSize ;
	if (!ByteCount || ByteCount != n*pKey->BlockSize) return false ;
   while (n--)
      {           
		if (e_CBC == CBC_SEND)
         {
         XorDataBlock(Temp, In, pKey->IV, pKey->BlockSize) ;
			if (!CryptDataBlock(pKey, Out, Temp, e_Cipher)) return false ;
   		memcpy(pKey->IV, Out, pKey->BlockSize) ;
         }
      else
			{
			if (!CryptDataBlock(pKey, Temp, In, e_Cipher)) return false ;
			XorDataBlock(Temp, Temp, pKey->IV, pKey->BlockSize) ;
			memcpy(pKey->IV, In, pKey->BlockSize) ;
			memcpy(Out, Temp, pKey->BlockSize) ;
			} ;
      In  += pKey->BlockSize ;
		Out += pKey->BlockSize ;
		} ;
   return true;
   }



LONG DES_Authenticate(HARDWARE *h, KEY *pKey)
   {
   static const unsigned char	AesChallengeCmd[]  = {0x90, 0xAA, 0x00, 0x00, 0x01, 0x00, 0x00} ;
   static const unsigned char Des3ChallengeCmd[] = {0x90, 0x1A, 0x00, 0x00, 0x01, 0x00, 0x00} ;
   static const unsigned char DesChallengeCmd[]  = {0x90, 0x0A, 0x00, 0x00, 0x01, 0x00, 0x00} ;
   static const unsigned char ChallengeResponseCmd[] = {0x90, 0xAF, 0x00, 0x00} ;
  	BYTE   RandB[32], RandA[32], RandAB[32], RandA_RL[16] ;
   DWORD  RandomSize, cmdLen ;
	BYTE   cmd[64] ;

   switch (pKey->keyType)
      {
      case KEY_AES:
		   memcpy(cmd, AesChallengeCmd, sizeof(AesChallengeCmd)) ;
         break ;

	   case KEY_3DES:
         memcpy(cmd, Des3ChallengeCmd, sizeof(Des3ChallengeCmd)) ;
         break ;

	   case KEY_DES:
		   memcpy(cmd, DesChallengeCmd, sizeof(DesChallengeCmd)) ;
         break ;

      default:
         return 0 ;
	   } ;

   memset(pKey->IV, 0, sizeof(pKey->IV)) ;
   cmd[5] = (unsigned char)(pKey->keyNum) ;

	// Request a random of 16 byte, but depending of the key the PICC may also return an 8 byte random
   RandomSize = (DWORD)sizeof(RandB) ;
	if (h->Push(cmd, sizeof(AesChallengeCmd), RandB, &RandomSize)) return 0 ;
	if (!CryptDataCBC(pKey, CBC_RECEIVE, KEY_DECIPHER, RandB, RandB, RandomSize)) return 0;

   CreateRnd(RandA, RandomSize) ;
	RotateBlockLeft(RandA + RandomSize, RandB, RandomSize) ;
	if (!CryptDataCBC(pKey, CBC_SEND, KEY_ENCIPHER, RandAB, RandA, RandomSize << 1)) return 0 ;

   memcpy(cmd, ChallengeResponseCmd, sizeof(ChallengeResponseCmd)) ;
   cmd[4] = (unsigned char)(RandomSize << 1) ;
	memcpy(&cmd[5], RandAB, RandomSize << 1) ;
	cmd[5 + (RandomSize << 1)] = 0 ;
	cmdLen = sizeof(RandAB) ;
	if (h->Push(cmd, 6 + (RandomSize << 1), RandAB, &cmdLen)) return 1 ;
   if (cmdLen != RandomSize) return 1 ;
	if (!CryptDataCBC(pKey, CBC_RECEIVE, KEY_DECIPHER, RandAB, RandAB, cmdLen)) return 1 ;
	RotateBlockLeft(RandA_RL, RandA, RandomSize) ;
   if (memcmp(RandAB, RandA_RL, RandomSize))	return 1 ;

	// The session key is composed from RandA and RndB
   memset(&SessionKey, 0, sizeof(KEY)) ;

	// Simple DES
   memcpy(&cmd[0], RandA, 4) ;
   memcpy(&cmd[4], RandB, 4) ;

   if (pKey->KeySize > 8)
      {
      switch (pKey->keyType)
		   {
		   case KEY_3DES:
            memcpy(&cmd[8], RandA + 4, 4) ;
            memcpy(&cmd[12], RandB + 4, 4) ;
			   break ;

		   case KEY_AES:
            memcpy(&cmd[8], RandA + 12, 4) ;
            memcpy(&cmd[12], RandB + 12, 4) ;
			   break ;

		   default:
			   break ;
		   } ;
      } ;

   if (pKey->keyType == KEY_AES)
      {
      SessionKey.keyType = KEY_AES ;
      SessionKey.BlockSize = 16 ;
      SessionKey.KeySize = 16 ;
      memcpy(SessionKey.key.Key, cmd, SessionKey.KeySize) ;
      }
   else
      {
      SessionKey.keyType = KEY_3DES ;
      SessionKey.BlockSize = 8 ;
      SessionKey.KeySize = 8 ;
      memcpy(SessionKey.key.Key, cmd, SessionKey.KeySize) ;
      DESSetKeyData(&SessionKey, 8) ;
      } ;
   
   LastAuthenticationKey = pKey->keyNum ;
   return 2 ;
   }


LONG DES_deleteApplication(HARDWARE *h, Application App)
   {
   static const unsigned char deleteApplicationCmd[] = {0x90, 0xDA, 0x00, 0x00, 0x03, 0x00, 0x00, 0x00, 0x00} ;
   static const unsigned char FEIG_deleteApplicationCmd[] = {0xC3, 0xDA, 0x00, 0x00} ;
	unsigned char cmd[32] ;
	DWORD  cmdLen = (DWORD)sizeof(cmd) ;

   memcpy(cmd, deleteApplicationCmd, sizeof(deleteApplicationCmd)) ;
   cmd[5] = (unsigned char)App ;
   cmd[6] = (unsigned char)(App >> 8) ;
   cmd[7] = (unsigned char)(App >> 16) ;
   return h->Push(cmd, sizeof(deleteApplicationCmd), cmd, &cmdLen) ;
   }


LONG DES_formatPICC(HARDWARE *h)
   {
   static const unsigned char formatPICCCmd[] = {0x90, 0xFC, 0x00, 0x00, 0x00} ;
   static const unsigned char DEIG_formatPICCCmd[] = {0xC3, 0xFC, 0x00, 0x00} ;
	unsigned char cmd[16];
	DWORD  cmdLen = (DWORD)sizeof(cmd) ;

   return h->Push(formatPICCCmd, (DWORD)sizeof(formatPICCCmd), cmd, &cmdLen) ;
   }


LONG DES_changeKey(HARDWARE *h, KEY *pNewKey, KEY *pKey, BOOLEAN SameKey)
   {
   static const uint8_t FEIG_KeyChangeCmd[] = {0xC3, 0xC4, 0x00} ;
   BYTE    Crypto[40], Crypto_enc[40], Command[2], cmd[80] ;
   size_t  CryptoLen ;
   BYTE    keyNum ;
   DWORD   crc, cmdLen ;

   keyNum = (BYTE)pNewKey->keyNum ;
   SameKey = (keyNum == (BYTE)LastAuthenticationKey) ;

  	// The type of key can only be changed for the PICC master key.
	// Applications must define their key type in CreateApplication().
   if (LastSelectedApplication == PICC_APPLICATION) keyNum |= (BYTE)pNewKey->keyType ;

   memset(Crypto, 0, sizeof(Crypto)) ;
   CryptoLen = pNewKey->KeySize ;
   memcpy(Crypto, pNewKey->key.Key, CryptoLen) ;
   if (!SameKey) XorDataBlock(Crypto, Crypto, pKey->key.Key, pNewKey->KeySize) ;

	// While DES stores the key version in bit 0 of the key bytes, AES transmits the version separately
   if (pNewKey->keyType == KEY_AES)
      {
      memcpy(&Crypto[CryptoLen], &pNewKey->key.KeyVersion, 1) ;
      CryptoLen++ ;
   	} ;

   Command[0] = 0xC4 ;
   Command[1] = keyNum ;
   crc = Crc32(Command, 2, 0xFFFFFFFF) ;
   crc = Crc32(Crypto, CryptoLen, crc) ;
   memcpy(&Crypto[CryptoLen], &crc, 4) ;
   CryptoLen += 4 ;

	if (!SameKey)
      {
      crc = Crc32(pNewKey->key.Key, 16, 0xFFFFFFFF) ;
      memcpy(&Crypto[CryptoLen], &crc, 4) ;
      CryptoLen += 4 ;
	   } ;

 	// For a blocksize of 16 byte (AES) the data length 24 is not valid -> increase to 32
	CryptoLen = (CryptoLen + 15) & ~(size_t)0xF ;
	CryptDataCBC(&SessionKey, CBC_SEND, KEY_ENCIPHER, Crypto_enc, Crypto, CryptoLen) ;

	if (SameKey) LastAuthenticationKey = NOT_AUTHENTICATED ;

	cmd[0] = 0x90;
	cmd[1] = 0xC4;
	cmd[2] = 0x00;
	cmd[3] = 0x00;
	cmd[4] = (BYTE)CryptoLen + 1 ;
   cmd[5] = keyNum ;
	memcpy(&cmd[6], Crypto_enc, CryptoLen) ;
	cmd[5 + cmd[4]] = 0x00;
	cmdLen = (DWORD)sizeof(cmd) ;
	return h->Push(cmd, 6 + cmd[4], cmd, &cmdLen) ;
   }


LONG DES_createApplication(HARDWARE *h, Application ApplicationID, BYTE NoAppKeys, CryptoMode cm)
   {
	/// CMD		AID	Key		Settings	NumOfKeys
	/// 0xCA	0xF21201				Bit 7 6	5 4 3 2 1 0	
	///									-------------------
	///							 		 1 0					Crypto Mode AES (command AA)
	///									 0 1					Crypto Mode 3KDES (command 1A)
	///									 0 0					Crypto Mode DES (command 0A)
	/// ------------------------------------------------------- 
	///										   0				No ISO file identifier
	///											1				ISO file identifier, 2 bytes
	/// ------------------------------------------------------- 
	///											  x				RFU
	/// ------------------------------------------------------- 
	///												  n n n n		Number of keys (max 14 = 0x0E)
	/// ------------------------------------------------------- 
	///						Bit 7 6	5 4 3 2 1 0 
	///						-------------------
	///                         x x x x							RFU
	///									----------------------- Configuration changeable
	///						            x							1 Configuration chnagable with master PICC key 
	///																0 Configuration frozen
	///									  --------------------- PICC master key	not required for create / delete
	///								      x							0 CreateApplication / DeleteApplication is permitted only with PICC master key authentication
	///									  x							1 CreateApplication is permitted without PICC master key authentication (default setting)
	///										------------------- Free directory list	access without PICC master key
	///										x						0 Successful PICC master key authentication is required for executing the GetApplicationIDs	and GetKeySettings commands
	///										x						1 GetApplicationIDs and GetKeySettings commands succeed independently of a preceding PICC master key authentication(default setting)
	///										  ----------------- Allow changing the PICC master key
	///										  x						0 PICC Master key is not changeable anymore (frozen)
	///										  x						1 PICC Master key is changeable (authentication with the current PICC master key necessary,	default setting)
	/// </Comment>
   static const unsigned char createApplicationCmd[] = {0x90, 0xCA, 0x00, 0x00, 0x05, 0x00, 0x00, 0x00, KS_DEFAULT, 0x00, 0x00} ;
   static const unsigned char FEIG_createApplicationCmd[] = {0xC3, 0xCA, 0x00, 0x00} ;
   unsigned char cmd[32];
	DWORD  cmdLen = (DWORD)sizeof(cmd) ;
   
	if (NoAppKeys > 14 || NoAppKeys == 0)
      {
      h->SetLastError(1) ;
      return 1 ;
      } ;

   memcpy(cmd, createApplicationCmd, sizeof(createApplicationCmd)) ;
   cmd[5] = (unsigned char)ApplicationID ;
   cmd[6] = (unsigned char)(ApplicationID >> 8) ;
   cmd[7] = (unsigned char)(ApplicationID >> 16) ;
   cmd[9] = (unsigned char)(cm | NoAppKeys) ;
   return h->Push(cmd, sizeof(createApplicationCmd), cmd, &cmdLen) ;
   }


LONG DES_createBackupFile(HARDWARE *h, BYTE FileNo, DESFireFileEncryption CommSettings, unsigned short AccessRights, int FileSize)
   {
   static const unsigned char createBackupFileCmd[] = {0x90, 0xCB, 0x00, 0x00, 0x07, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00} ;
   static const unsigned char FEIG_createBackupFileCmd[] = {0xC3, 0xCB, 0x00, 0x00} ;
   unsigned char cmd[32];
	DWORD  cmdLen = (DWORD)sizeof(cmd) ;

   memcpy(cmd, createBackupFileCmd, sizeof(createBackupFileCmd)) ;
   cmd[5] = FileNo;
   cmd[6] = (unsigned char)CommSettings;
   cmd[7] = (unsigned char)AccessRights ;
   cmd[8] = (unsigned char)(AccessRights >> 8) ;
   cmd[9] = (unsigned char)FileSize ;
   cmd[10] = (unsigned char)(FileSize >> 8) ;
   cmd[11] = (unsigned char)(FileSize >> 16);
   return h->Push(cmd, sizeof(createBackupFileCmd), cmd, &cmdLen);
   }


static unsigned short PackFileAccessRights(DesfireFileDef *pTab)
   {
   if (pTab) return (unsigned short)((pTab->access_read << 12) | (pTab->access_write << 8) | (pTab->access_rw << 4) | (pTab->access_change)) ;
   return 0 ;
   }


LONG DES_createGFIBackupFile(HARDWARE *h, BYTE FileNo, Application App)
   {
   DesfireFileDef *pTab = FindApplicationFile(FileNo, App) ;
   if (pTab)
      return DES_createBackupFile(h, FileNo, CM_PLAIN, PackFileAccessRights(pTab), pTab->fileSize) ;
   h->SetLastError(1) ;
   return 1 ;
   }



LONG DES_createGFIStandardFile(HARDWARE *h, BYTE FileNo, Application App)
   {
   DesfireFileDef *pTab = FindApplicationFile(FileNo, App) ;
   if (pTab)
      return DES_createStandardDataFile(h, FileNo, CM_PLAIN, PackFileAccessRights(pTab), pTab->fileSize) ;
   h->SetLastError(1) ;
   return 1 ;
   }


LONG DES_createStandardDataFile(HARDWARE *h, BYTE FileNo, DESFireFileEncryption CommSettings, unsigned short AccessRights, int FileSize)
   {
   static const uint8_t createStandardDataFileCmd[] = {0x90, 0xCD, 0x00, 0x00, 0x07, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00} ;
   static const uint8_t FEIG_createStandardDataFileCmd[] = {0xC3, 0xCD, 0x00, 0x00} ;
	unsigned char cmd[50];
	DWORD  cmdLen = (DWORD)sizeof(cmd) ;

   memcpy(cmd, createStandardDataFileCmd, sizeof(createStandardDataFileCmd)) ;
   cmd[5] = FileNo;
   cmd[6] = (unsigned char)CommSettings;
   cmd[7] = (unsigned char)AccessRights ;
   cmd[8] = (unsigned char)(AccessRights >> 8);
   cmd[9] = (unsigned char)FileSize ;
   cmd[10] = (unsigned char)(FileSize >> 8) ;
   cmd[11] = (unsigned char)(FileSize >> 16);
   return h->Push(cmd, sizeof(createStandardDataFileCmd), cmd, &cmdLen) ;
   }


LONG DES_createGFICyclicFile(HARDWARE *h, BYTE FileNo, Application App)
   {
   DesfireFileDef *pTab = FindApplicationFile(FileNo, App) ;
   if (pTab)
      return DES_createCyclicRecordFile(h, FileNo, CM_PLAIN, PackFileAccessRights(pTab), pTab->fileSize, pTab->maxRecords);
   h->SetLastError(1) ;
   return 1 ;
   }


LONG DES_createCyclicRecordFile(HARDWARE *h, BYTE FileNo, DESFireFileEncryption CommSettings, unsigned short AccessRights, unsigned int RecordSize, unsigned int  MaxRecords)
   {
   static const uint8_t createCyclicRecordFileCmd[] = {0x90, 0xC0, 0x00, 0x00, 0x0A, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00} ;
   static const uint8_t FEIG_createCyclicRecordFileCmd[] = {0xC3, 0xC0, 0x00, 0x00} ;
   unsigned char cmd[50];
	DWORD  cmdLen = (DWORD)sizeof(cmd) ;

   memcpy(cmd, createCyclicRecordFileCmd, sizeof(createCyclicRecordFileCmd)) ;
   cmd[5]  = FileNo;
   cmd[6]  = (unsigned char)CommSettings;
   cmd[7]  = (unsigned char)AccessRights ;
   cmd[8]  = (unsigned char)(AccessRights >> 8);
   cmd[9]  = (unsigned char)RecordSize ;
   cmd[10] = (unsigned char)(RecordSize >> 8) ;
   cmd[11] = (unsigned char)(RecordSize >> 16);
   cmd[12] = (unsigned char)MaxRecords ;
   cmd[13] = (unsigned char)(MaxRecords >> 8) ;
   cmd[14] = (unsigned char)(MaxRecords >> 16) ;
   return h->Push(cmd, sizeof(createCyclicRecordFileCmd), cmd, &cmdLen) ;
   }


LONG DES_commit(HARDWARE *h)
   {
   static const unsigned char commitDESCmd[] = {0x90, 0xC7, 0x00, 0x00, 0x00} ;
   static const unsigned char FEIG_commitDESCmd[] = {0xC3, 0xC7, 0x00, 0x00} ;
   unsigned char cmd[16] ;
	DWORD  cmdLen = sizeof(cmd) ;

   if (!commit) return 0 ;
   commit = FALSE ;
   return h->Push(commitDESCmd, sizeof(commitDESCmd), cmd, &cmdLen) ;
   }


// Debit
LONG DES_Debit(HARDWARE *h, BYTE FileNo, unsigned int Value)
   {
   static const unsigned char DebitCmd[] = {0x90, 0xDC, 0x00, 0x00, 0x05, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00} ;
   unsigned char cmd[32];
	DWORD  cmdLen = (DWORD)sizeof(cmd) ;

   memcpy(cmd, DebitCmd, sizeof(DebitCmd)) ;
	cmd[5] = FileNo;
	cmd[6] = (unsigned char)Value ;
	cmd[7] = (unsigned char)(Value >> 8) ;
	cmd[8] = (unsigned char)(Value >> 16) ;
	cmd[9] = (unsigned char)(Value >> 24) ;
	return h->Push(cmd, sizeof(DebitCmd), cmd, &cmdLen) ;
   }


// Credit
LONG DES_Credit(HARDWARE *h, BYTE FileNo, unsigned int Value)
   {
   static const unsigned char CreditCmd[] = {0x90, 0x0C, 0x00, 0x00, 0x05, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00} ;
	unsigned char cmd[32];
	DWORD  cmdLen = (DWORD)sizeof(cmd) ;

   memcpy(cmd, CreditCmd, sizeof(CreditCmd)) ;
	cmd[5] = FileNo;
	cmd[6] = (unsigned char)Value ;
	cmd[7] = (unsigned char)(Value >> 8) ;
	cmd[8] = (unsigned char)(Value >> 16) ;
	cmd[9] = (unsigned char)(Value >> 24) ;
	return h->Push(cmd, sizeof(CreditCmd), cmd, &cmdLen) ;
   }


// CreateGFIValueFile
LONG DES_createGFIValueFile(HARDWARE *h, BYTE FileNo, Application App)
   {
   DesfireFileDef *pTab = FindApplicationFile(FileNo, App) ;
   unsigned char flags = 0 ;
   if (pTab)
      {
      if (pTab->LimitedCreditEnabled) flags = 1 ;
      if (pTab->FreeReadEnabled) flags |= 0x2 ;
      return DES_createValueFile(h, FileNo, CM_PLAIN, PackFileAccessRights(pTab), pTab->fileSize, pTab->maxRecords, pTab->Value, flags) ;
      } ;
   h->SetLastError(1) ;
   return 1 ;
   }


// CreateValueFile
LONG DES_createValueFile(HARDWARE *h, BYTE FileNo, DESFireFileEncryption CommSettings, unsigned short AccessRights, int LowerLimit, int UpperLimit, int Value, unsigned char LimitedCreditEnabled)
   {
   static const uint8_t createValueFileCmd[] = {0x90, 0xCC, 0x00, 0x00, 17} ;
   static const uint8_t FEIG_createValueFileCmd[] = {0xC3, 0xCC, 0x00, 0x00} ;
   unsigned char cmd[32];
	DWORD  cmdLen = (DWORD)sizeof(cmd) ;

   memcpy(cmd, createValueFileCmd, sizeof(createValueFileCmd)) ;
   cmd[5]  = FileNo;
   cmd[6]  = (unsigned char)CommSettings;
   cmd[7]  = (unsigned char)AccessRights ;
   cmd[8]  = (unsigned char)(AccessRights >> 8) ;
   cmd[9]  = (unsigned char)LowerLimit ;
   cmd[10] = (unsigned char)(LowerLimit >> 8) ;
   cmd[11] = (unsigned char)(LowerLimit >> 16) ;
   cmd[12] = (unsigned char)(LowerLimit >> 24) ;
   cmd[13] = (unsigned char)UpperLimit ;
   cmd[14] = (unsigned char)(UpperLimit >> 8) ;
   cmd[15] = (unsigned char)(UpperLimit >> 16) ;
   cmd[16] = (unsigned char)(UpperLimit >> 24) ;
   cmd[17] = (unsigned char)Value ;
   cmd[18] = (unsigned char)(Value >> 8) ;
   cmd[19] = (unsigned char)(Value >> 16) ;
   cmd[20] = (unsigned char)(Value >> 24) ;
     cmd[21] = (unsigned char)LimitedCreditEnabled ;
   cmd[22] = 0x00 ;
   return h->Push(cmd, 23, cmd, &cmdLen);
   }


/// Get Value
LONG DES_getValue(HARDWARE *h, BYTE FileNo, int *Value)
   {
   static const unsigned char getValueCmd[] = {0x90, 0x6C, 0x00, 0x00, 0x01, 0x00, 0x00} ;
   static const unsigned char FEIG_getValueCmd[] = {0xC3, 0x6C, 0x00, 0x00, 0x00, 0x00} ;
	unsigned char cmd[32];
	DWORD  cmdLen = (DWORD)sizeof(cmd) ;
   LONG   rc ;

   memcpy(cmd, getValueCmd, sizeof(getValueCmd)) ;
   cmd[5] = FileNo;
   if ((rc = h->Push(cmd, sizeof(getValueCmd), cmd, &cmdLen))) return rc ;
   memcpy(Value, cmd, 4) ;
	return 0 ;
   }


void DES_setCommit(BOOLEAN b)
   {
   commit = b ;
   }


LONG DES_write(HARDWARE *h, BYTE FileNo, BYTE *data, size_t len, DWORD SegmentOffset, WriteMode WM)
   {
   static const unsigned char writeDESCmd[] = {0x90, 0x00, 0x00, 0x00} ;
   static const unsigned char FEIG_writeDESCmd[] = {0xC3, 0x00, 0x00, 0x00, 0x00, 0x00} ;
   unsigned char cmd[128] ;
	DWORD  cmdLen = (DWORD)sizeof(cmd) ;

   memcpy(cmd, writeDESCmd, sizeof(writeDESCmd)) ;
   cmd[1] = WM ;
   cmd[4] = (unsigned char)(7 + len) ;
   cmd[5] = FileNo ;
   cmd[6] = (unsigned char)SegmentOffset ;
   cmd[7] = (unsigned char)(SegmentOffset >> 8) ;
   cmd[8] = (unsigned char)(SegmentOffset >> 16) ;
   cmd[9] = (unsigned char)len ;
   cmd[10] = (unsigned char)(len >> 8) ;
   cmd[11] = (unsigned char)(len >> 16) ;
   memcpy(&cmd[12], data, len) ;
   cmd[12 + len] = 0 ;
   return h->Push(cmd, 13 + len, cmd, &cmdLen) ;
	}


LONG DES_writeRecord(HARDWARE *h, BYTE FileNo, BYTE *data, size_t len)
   {
	return DES_write(h, FileNo, data, len, 0, WriteRecord) ;
   }


LONG DES_writeData(HARDWARE *h, BYTE FileNo, BYTE *data, size_t len)
   {
   size_t nBytes ;
   size_t offset = 0 ;
   LONG   rc ;

   while (len)
      {
      if (len > 32)
         nBytes = 32 ;
      else
         nBytes = len ;
      if ((rc = DES_write(h, FileNo, data, nBytes, (DWORD)offset, WriteData))) return rc ;
      len -= nBytes ;
      offset += nBytes ;
      } ;
   return 0 ;
   }


// Get file info from card
LONG DES_fileInfo(HARDWARE *h, BYTE FileNo, FILE_DESCR *pFile)
   {
   static const unsigned char getFileInfoCmd[] = {0x90, 0xF5, 0x00, 0x00, 0x01, 0x00, 0x00} ;
   static const unsigned char FEIG_getFileInfoCmd[] = {0xC3, 0xF5, 0x00, 0x00, 0x00} ;
   unsigned char cmd[32];
	DWORD  cmdLen = (DWORD)sizeof(cmd) ;
   LONG   rc ;

   memcpy(cmd, getFileInfoCmd, sizeof(getFileInfoCmd)) ;
   cmd[5] = FileNo;
   if ((rc = h->Push(cmd, sizeof(getFileInfoCmd), cmd, &cmdLen))) return rc ;
   pFile->fileNum = FileNo ;
   pFile->FileType = (DESFireFileType)cmd[0] ;
   pFile->access_rw = (DESFireAccessRights)(cmd[2] >> 4) ;
   pFile->access_change = (DESFireAccessRights)(cmd[2] & 0xF) ;
   pFile->access_read = (DESFireAccessRights)(cmd[3] >> 4) ;
   pFile->access_write = (DESFireAccessRights)(cmd[3] & 0xF) ;
   pFile->fileSize = (DWORD)(cmd[4]) | ((DWORD)(cmd[5]) << 8) | ((DWORD)(cmd[6]) << 16);
   if (pFile->FileType == MDFT_CYCLIC_FILE_WITH_BACKUP)
      {
      pFile->maxRecords = (DWORD)(cmd[7]) | ((DWORD)(cmd[8]) << 8) | ((DWORD)(cmd[9]) << 16) ;
      pFile->curRecords = (DWORD)(cmd[10]) | ((DWORD)(cmd[11]) << 8) | ((DWORD)(cmd[12]) << 16) ;
      } ;
   return 0 ;
   }


// DesFire error codes
const char* DES_ErrorCode(uint16_t sw1sw2)
   {
   static char buf[64] ;

   switch (sw1sw2 & 0xFF)
      {
      case 0x00:
         return "DES: Success" ;
      case 0x0C:
         return "DES: No changes. CommitTransaction/AbortTransaction not necessary" ;
      case 0x0E:
         return "DES: Insufficient NV-Memory to complete command" ;
      case 0x1C:
         return "DES: Command code not supported" ;
      case 0x1E:
         return "DES: CRC or MAC does not match data. Padding bytes not valid" ;
      case 0x40:
         return "DES: Invalid key number specified" ;
      case 0x7E:
         return "DES: Length of command string invalid" ;
      case 0x9D:
         return "DES: Permission denied" ;
      case 0x9E:
         return "DES: Value of the parameter(s) invalid" ;
      case 0xA0:
         return "DES: Requested AID not present on PICC" ;
      case 0xA1:
         return "DES: Unrecoverable error within application, application will be disabled" ;
      case 0xAE:
         return "DES: Authentication error" ;
      case 0xAF:
         return "DES: Additional data frame is expected to be sent" ;
      case 0xBE:
         return "DES: Attempt to read/write data from/to beyond the file's/record's limits.\r\nAttempt to exceed the limits of a value file" ;
      case 0xC1:
         return "DES: Unrecoverable error within PICC, PICC will be disabled" ;
      case 0xCA:
         return "DES: Previous Command was not fully completed.\r\nNot all Frames were requested or provided by the PCD" ;
      case 0xCD:
         return "DES: PICC was disabled by an unrecoverable error" ;
      case 0xCE:
         return "DES: Number of Applications limited to 28.\r\nNo additional CreateApplication possible" ;
      case 0xDE:
         return "DES: File/application with same number already exists" ;
      case 0xEE:
         return "DES: Could not complete NV-write operation due to loss of power" ;
      case 0xF0:
         return "DES: Specified file number does not exist" ;
      case 0xF1:
         return "DES: Unrecoverable error within file, file will be disabled" ;
      default:
         sprintf_s(buf, sizeof(buf), "Unknown error 0x%X\n", sw1sw2) ;
         return buf ;
      } ;
   }
