// PC Smart Card Interface

#include "stdafx.h"
#include "winscard.h"

extern volatile BOOLEAN run ;

static const unsigned char ATR_CLASSIC[]      = {0x3B, 0x8F, 0x80, 0x01, 0x80, 0x4F, 0x0C, 0xA0, 0x00, 0x00, 0x03, 0x06, 0x03, 0x00, 0x01, 0x00, 0x00, 0x00, 0x00, 0x6A} ;
static const unsigned char ATR_CLASSIC2[]     = {0x3B, 0x8F, 0x80, 0x01, 0x80, 0x4F, 0x0C, 0xA0, 0x00, 0x00, 0x03, 0x06, 0x03, 0xFF, 0x88, 0x00, 0x00, 0x00, 0x00, 0x1C} ;

static const unsigned char ATR_ULTRALIGHT[]   = {0x3B, 0x8F, 0x80, 0x01, 0x80, 0x4F, 0x0C, 0xA0, 0x00, 0x00, 0x03, 0x06, 0x03, 0x00, 0x03, 0x00, 0x00, 0x00, 0x00, 0x68} ;
static const unsigned char ATR_ULTRALIGHT_C[] = {0x3B, 0x8F, 0x80, 0x01, 0x80, 0x4F, 0x0C, 0xA0, 0x00, 0x00, 0x03, 0x06, 0x03, 0x00, 0x3A, 0x00, 0x00, 0x00, 0x00, 0x51} ;
static const unsigned char ATR_DESFIRE[]      = {0x3B, 0x81, 0x80, 0x01, 0x80, 0x80} ;

static char* SC_GetDevList(DWORD *lpnReaders) ;
static LONG  SC_FreeDevList(void) ;
static LONG  SC_Initialize(DWORD devNum) ;
static LONG  SC_Open(DWORD devNum) ;
static LONG  SC_Close(void) ;
static HANDLE SC_Detect(DWORD devNum, HWND hwnd) ;
static LONG  SC_Push(const unsigned char *pcmd, size_t cmdLen, unsigned char *poutBuf, DWORD *poutLen) ;
static char* getUUID(void) ;
static char* getATR(void) ;
static WORD  getSW12(void) ;
static const char* SC_GetLastError(void) ;
static void  SC_SetLastError(LONG error) ;
static BYTE  SC_GetCardType(void) ;
static LONG  ULC_Authenticate(BYTE sector, const unsigned char *key) ;
static LONG  Mifare_Auth(uint8_t block) ;


static HARDWARE pcsc = {1, SC_GetDevList, SC_FreeDevList, SC_Open, SC_Close, SC_Detect, SC_Push, getUUID, getATR, getSW12, SC_SetLastError, SC_GetLastError, SC_GetCardType, ULC_Authenticate, NULL, Mifare_Auth} ;

static LONG ErrorCode = 0 ;
static SCARDCONTEXT hContext = 0 ;
static SCARDHANDLE  hCard = 0 ;
static LPSTR  pmszReaders = NULL ;
static DWORD  nReaders = 0 ;
static char   CardATR[64] ;
static char   CardUUID[32] ;
static DWORD  ActiveProtocol ;
static WORD   SW12 ;
static BYTE   CardType = 0 ;


HARDWARE* GetPCSCIntrface(void)
   {
   return &pcsc ;
   }


static char* SC_GetDevList(DWORD *lpnReaders)
   {
   LPSTR  p ;
   ULONG  ResponseLength ;
   
   hContext = *lpnReaders = 0 ;
   ErrorCode = SCardEstablishContext(SCARD_SCOPE_USER, NULL, NULL, &hContext) ;
	if (ErrorCode != SCARD_S_SUCCESS) return NULL ;
	
   pmszReaders = NULL ;
   ResponseLength = SCARD_AUTOALLOCATE ;
   ErrorCode = SCardListReaders(hContext, 0, (LPSTR)&pmszReaders, &ResponseLength) ;
   if (ErrorCode != SCARD_S_SUCCESS)
      {
      SCardReleaseContext(hContext) ;
      hContext = 0 ;
      return NULL ;
      } ;
   p = pmszReaders ;
   nReaders = 0 ;
   while (*p)
      {
      p += 1 + strlen(p) ;
      nReaders++ ;
      } ;
   *lpnReaders = nReaders ;
   return pmszReaders ;
   }


static LONG SC_FreeDevList(void)
   {
   if (pmszReaders && hContext)
      {
      SCardFreeMemory(hContext, pmszReaders) ;
      pmszReaders = NULL ;
      } ;
   if (hContext)
      {
      SCardReleaseContext(hContext) ;
      hContext = 0 ;
      } ;
   return 0 ;
   }



static LONG SC_Open(DWORD devNum)
   {
   static const unsigned char GetUUIDCmd[] = {0xFF, 0xCA, 0x00, 0x00, 0x00} ;
   static const unsigned char MifareRead[] = {0xFF, 0xB0, 0x00, 0x00, 0x10} ;
   DWORD  ReaderNameLength, CardState, UIDLen ;
   char  *p ;
   char   buf[128] ;
   BYTE   atr[32] ;

   if (hContext == 0 || pmszReaders == NULL || devNum >= nReaders)
      {
      ErrorCode = 1 ;
      return ErrorCode ;
      } ;

   p = pmszReaders ;
   while (devNum--) p += strlen(p) + 1 ;
   if (hCard) SCardDisconnect(hCard, SCARD_EJECT_CARD) ;
   hCard = 0 ;
   ErrorCode = SCardConnect(hContext, p, SCARD_SHARE_SHARED, SCARD_PROTOCOL_T0|SCARD_PROTOCOL_T1, &hCard, &ActiveProtocol) ;
   if (ErrorCode != SCARD_S_SUCCESS) return ErrorCode ;

	//SCARD_ABSENT	   	1	There is no card in the reader.
	//SCARD_PRESENT		2	There is a card in the reader, but it has not been moved into position for use.
	//SCARD_SWALLOWED	   3	There is a card in the reader in position for use.The card is not powered.
	//SCARD_POWERED		4	Power is being provided to the card, but the reader driver is unaware of the mode of the card.
	//SCARD_NEGOTIABLE	5	The card has been reset and is awaiting PTS negotiation.
	//SCARD_SPECIFIC	   6	The card has been reset and specific communication protocols have been established.
	
	//SCARD_PROTOCOL_RAW	The Raw Transfer protocol is in use.
	//SCARD_PROTOCOL_T0		The ISO 7816 / 3 T = 0 protocol is in use.
	//SCARD_PROTOCOL_T1		The ISO 7816 / 3 T = 1 protocol is in use
	
   ErrorCode = SCardReconnect(hCard, SCARD_SHARE_SHARED, SCARD_PROTOCOL_T0 | SCARD_PROTOCOL_T1,	SCARD_UNPOWER_CARD, &ActiveProtocol) ;
	if (ErrorCode != SCARD_S_SUCCESS)
      {
		return ErrorCode ;
	   } ;

   ReaderNameLength = (DWORD)sizeof(buf) ;
   UIDLen = sizeof(atr) ;
   ErrorCode = SCardStatus(hCard, buf, &ReaderNameLength, &CardState, &ActiveProtocol, atr, &UIDLen) ;
   if (ErrorCode != SCARD_S_SUCCESS) return ErrorCode ;
   HexToStr(atr, UIDLen, CardATR) ;

   if (UIDLen == sizeof(ATR_DESFIRE) && !memcmp(atr, ATR_DESFIRE, sizeof(ATR_DESFIRE)))
      CardType = CARD_TYPE_DESFIRE ;
   else if (UIDLen == sizeof(ATR_ULTRALIGHT) && !memcmp(atr, ATR_ULTRALIGHT, sizeof(ATR_ULTRALIGHT)))
      CardType = CARD_TYPE_ULTRALIGHT ;
   else if (UIDLen == sizeof(ATR_ULTRALIGHT_C) && !memcmp(atr, ATR_ULTRALIGHT_C, sizeof(ATR_ULTRALIGHT_C)))
      CardType = CARD_TYPE_ULTRALIGHT_C ;
   else if (UIDLen == sizeof(ATR_CLASSIC) && (!memcmp(atr, ATR_CLASSIC, sizeof(ATR_CLASSIC)) || !memcmp(atr, ATR_CLASSIC2, sizeof(ATR_CLASSIC2))))
      CardType = CARD_TYPE_CLASSIC ;
   else
      CardType = 0 ;

   UIDLen = (DWORD)sizeof(buf) ;
	ErrorCode = SC_Push(GetUUIDCmd, (DWORD)sizeof(GetUUIDCmd), (unsigned char*)buf, &UIDLen) ;
   if (ErrorCode != SCARD_S_SUCCESS)
      CardUUID[0] = 0 ;
   else
      HexToStr((unsigned char*)buf, UIDLen, CardUUID) ;
   return 0 ;
   }


static LONG SC_Close(void)
   {
   if (hCard == 0)
      ErrorCode = 1 ;
   else
      {
      ErrorCode = SCardDisconnect(hCard, SCARD_EJECT_CARD) ;
      hCard = 0 ;
      } ;
   return ErrorCode ;
   }


LONG SC_ReOpen(void)
   {
   return SCardReconnect(hCard, SCARD_SHARE_SHARED, SCARD_PROTOCOL_T0 | SCARD_PROTOCOL_T1, SCARD_UNPOWER_CARD, &ActiveProtocol) ;
   }


static DWORD WINAPI SC_DetectThread(void *lpParam)
   {
   DWORD  n, devNum = ((DETECT_PARAMS*)lpParam)->devNum ;
   HWND   hwnd = ((DETECT_PARAMS*)lpParam)->hwnd ;
   SCARD_READERSTATE readerState ;
   CARD_STATE card_state = CARD_STATE_UNKNOWN ;
   LONG   ErrorCode ;
   char  *p ;

   // Parameters check
   if (hContext == 0 || pmszReaders == NULL || devNum >= nReaders)
      {
      ErrorCode = 1 ;
    ERREXIT:
      SetEvent(((DETECT_PARAMS*)lpParam)->hEvent) ;
      return 0 ;
      } ;

   SetThreadPriority(GetCurrentThread(), THREAD_PRIORITY_TIME_CRITICAL) ;
   // Setup reader's parameters
   memset(&readerState, 0, sizeof(readerState)) ;
   p = pmszReaders ;
   n = devNum ;
   while (n--) p += strlen(p) + 1 ;
   readerState.szReader = p ;

   // Detect current state
  	readerState.dwCurrentState = SCARD_STATE_UNAWARE ;
	readerState.dwEventState = SCARD_STATE_UNKNOWN ;
   ErrorCode = SCardGetStatusChange(hContext, 0, &readerState, 1) ;
   if (ErrorCode != SCARD_S_SUCCESS) goto ERREXIT ;
   if ((readerState.dwEventState & SCARD_STATE_PRESENT) == SCARD_STATE_PRESENT)
      card_state = CARD_STATE_PRESENT ;
   else
      card_state = CARD_STATE_EMPTY ;

   // Set signal that we are ready
   run = TRUE ;
   SetEvent(((DETECT_PARAMS*)lpParam)->hEvent) ;

   // Initial state
   PostMessage(hwnd, WM_SC_CARD_STATUS, card_state, 0) ;

   // Run here until we get external signal to terminate
	for (;;)
	   {
		ErrorCode = SCardGetStatusChange(hContext, 500, &readerState, 1) ;
      if (!run) break ;
      if (ErrorCode == SCARD_E_TIMEOUT) continue ;
      if (ErrorCode != SCARD_S_SUCCESS) continue ;
		if (card_state == CARD_STATE_PRESENT)
         {
			if ((readerState.dwEventState & SCARD_STATE_PRESENT) != SCARD_STATE_PRESENT)
				card_state = CARD_STATE_EMPTY ;
         else
            continue ;
		   }
      else
         {
         if ((readerState.dwEventState & SCARD_STATE_PRESENT) == SCARD_STATE_PRESENT)
   			card_state = CARD_STATE_PRESENT ;
         else
            continue ;
         } ;

      // Verify
      Sleep(250) ;
      readerState.dwCurrentState = SCARD_STATE_UNAWARE ;
	   readerState.dwEventState = SCARD_STATE_UNKNOWN ;
      ErrorCode = SCardGetStatusChange(hContext, 0, &readerState, 1) ;
      if (ErrorCode != SCARD_S_SUCCESS) continue ;
      if (card_state == CARD_STATE_PRESENT)
         {
         if ((readerState.dwEventState & SCARD_STATE_PRESENT) != SCARD_STATE_PRESENT) continue ;
         }
      else
         {
         if ((readerState.dwEventState & SCARD_STATE_PRESENT) == SCARD_STATE_PRESENT) continue ;
         } ;

      PostMessage(hwnd, WM_SC_CARD_STATUS, card_state, 0) ;
	   } ;
   SC_Close() ;
   return 0 ;
   } 



static HANDLE SC_Detect(DWORD devNum, HWND hwnd)
   {
   DETECT_PARAMS params ;
   HANDLE hThread ;

   run = FALSE ;
   params.devNum = devNum ;
   params.hwnd = hwnd ;
   params.hEvent = CreateEvent(NULL, FALSE, FALSE, NULL) ;
   if (!params.hEvent) return NULL ;
   hThread = CreateThread(NULL, 0, SC_DetectThread, &params, 0, NULL) ;
   if (hThread) WaitForSingleObject(params.hEvent, INFINITE) ;
   CloseHandle(params.hEvent) ;
   if (run) return hThread ;
   CloseHandle(hThread) ;
   return NULL ;
   }



static LONG SC_Push(const unsigned char *pcmd, size_t cmdLen, unsigned char *poutBuf, DWORD *poutLen)
   {
	SCARD_IO_REQUEST IO_Request ;
   DWORD  outLen = *poutLen ;

   *poutLen = 0 ;
	IO_Request.dwProtocol = ActiveProtocol ;
	IO_Request.cbPciLength = (DWORD)sizeof(SCARD_IO_REQUEST) ;
   ErrorCode = SCardTransmit(hCard, &IO_Request, pcmd, (DWORD)cmdLen, NULL, poutBuf, &outLen) ;
  	if (ErrorCode != SCARD_S_SUCCESS) return ErrorCode ;
   if (outLen < 2)
      {
      ErrorCode = 2 ;
      return ErrorCode ;
      } ;
   SW12 = ((WORD)(poutBuf[outLen - 2] << 8) | (WORD)(poutBuf[outLen - 1])) ;
   if (SW12 != 0x9000 && SW12 != 0x9100 && SW12 != 0x91AF && SW12 != 0x910c)
      {
      ErrorCode = 3 ;
      return ErrorCode ;
      } ;

   *poutLen = outLen - 2 ;
   return 0 ;
   }


static char* getUUID(void)
   {
   return CardUUID ;
   }


static char* getATR(void)
   {
   return CardATR ;
   }


static WORD getSW12(void)
   {
   return SW12 ;
   }


static void SC_SetLastError(LONG error)
   {
   ErrorCode = error ;
   }


static BYTE SC_GetCardType(void)
   {
   return CardType ;
   }


BOOLEAN CryptDataCBC(KEY *pKey, DESFireCBC e_CBC, DESFireCipher e_Cipher, unsigned char* Out, const unsigned char* In, size_t ByteCount) ;


static LONG  ULC_Authenticate(BYTE sector, const unsigned char *key)
   {
   static const unsigned char Des3ChallengeCmd[]     = {0xFF, 0x00, 0x00, 0x00, 0x02, 0x1A, 0x00} ;
   static const unsigned char ChallengeResponseCmd[] = {0xFF, 0x00, 0x00, 0x00, 17, 0xAF} ;
   LONG  rc ;
   KEY   DESKey ;
   BYTE  RandB[32], RandA[32], RandAB[32], RandA_RL[16] ;
   DWORD RandomSize, cmdLen ;
	BYTE  cmd[64] ;

   memset(&DESKey, 0, sizeof(KEY)) ;
   DESKey.keyType = KEY_3DES ;
   DESKey.BlockSize = 8 ;
   memcpy(&DESKey.key.Key, key, 16) ;
   DESSetKeyData(&DESKey, 16) ;

	// Request a random 8 bytes
   RandomSize = (DWORD)sizeof(RandB) ;
   if (rc = SC_Push(Des3ChallengeCmd, sizeof(Des3ChallengeCmd), cmd, &RandomSize)) return rc ;
   if (RandomSize != 9 || cmd[0] != 0xAF) return 1 ;
   if (!CryptDataCBC(&DESKey, CBC_RECEIVE, KEY_DECIPHER, RandB, &cmd[1], 8)) return 1 ;
   CreateRnd(RandA, 8) ;
	RotateBlockLeft(&RandA[8], RandB, 8) ;
	if (!CryptDataCBC(&DESKey, CBC_SEND, KEY_ENCIPHER, RandAB, RandA, 16)) return 1 ;
   memcpy(cmd, ChallengeResponseCmd, sizeof(ChallengeResponseCmd)) ;
	memcpy(&cmd[sizeof(ChallengeResponseCmd)], RandAB, 16) ;
	cmdLen = sizeof(RandAB) ;
	if (rc = SC_Push(cmd, 16 + sizeof(ChallengeResponseCmd), RandAB, &cmdLen)) return rc ;
   if (cmdLen != 9 || RandAB[0] != 0) return 1 ;
	if (!CryptDataCBC(&DESKey, CBC_RECEIVE, KEY_DECIPHER, RandAB, RandAB+1, 16)) return 1 ;
	RotateBlockLeft(RandA_RL, RandA, 8) ;
   if (memcmp(RandAB, RandA_RL, 8))	return 1 ;
   return 0 ;
   }


#define MC_KeyALoc  0

static LONG Mifare_Auth(uint8_t block)
   {
   static const uint8_t LoadKeys[]   = {0xFF,0x82,0x00,MC_KeyALoc,0x06} ;
   static const uint8_t MifareAuth[] = {0xFF,0x86,0x00,0x00,0x05,0x01,0x00,0x00,0x60,MC_KeyALoc} ;
   uint8_t buf[64] ;
   DWORD   len ;
   LONG    rc ;

   // MIFARE Keys
   // Sector A
   memcpy(buf, LoadKeys, sizeof(LoadKeys)) ;
   buf[3] = MC_KeyALoc ;
   memcpy(buf + sizeof(LoadKeys), CLASSIC_KEYS_A, 6) ;
   len = sizeof(buf) ;
   rc = SC_Push(buf, sizeof(LoadKeys) + 6, buf, &len) ;
   if (rc) return rc ;
   // Sector B
   memcpy(buf, LoadKeys, sizeof(LoadKeys)) ;
   buf[3] = MC_KeyALoc+1 ;
   memcpy(buf + sizeof(LoadKeys), CLASSIC_KEYS_B, 6) ;
   len = sizeof(buf) ;
   rc = SC_Push(buf, sizeof(LoadKeys) + 6, buf, &len) ;
   if (rc) return rc ;

   memcpy(buf, MifareAuth, sizeof(MifareAuth)) ;
   buf[7] = block ;
   len = sizeof(buf) ;
   return SC_Push(buf, sizeof(MifareAuth), buf, &len) ;
   }


const char* DecodeError(DWORD SW12)
   {
   if (0x91 != (SW12 >> 8)) return "" ;

      // DesFire error codes
   switch (SW12 & 0xFF)
      {
      case 0x00:  return "" ;
      case 0x0C:  return "No changes. CommitTransaction/AbortTransaction not necessary" ;
      case 0x0E:  return "Insufficient NV-Memory to complete command" ;
      case 0x1C:  return "Command code not supported" ;
      case 0x1E:  return "CRC or MAC does not match data. Padding bytes not valid" ;
      case 0x40:  return "Invalid key number specified" ;
      case 0x7E:  return "Length of command string invalid" ;
      case 0x9D:  return "Permission denied" ;
      case 0x9E:  return "Value of the parameter(s) invalid" ;
      case 0xA0:  return "Requested AID not present on PICC" ;
      case 0xA1:  return "Unrecoverable error within application, application will be disabled" ;
      case 0xAE:  return "Authentication error" ;
      case 0xAF:  return "Additional data frame is expected to be sent" ;
      case 0xBE:  return "Attempt to read/write data from/to beyond the file's/record's limits.\r\nAttempt to exceed the limits of a value file." ;
      case 0xC1:  return "Unrecoverable error within PICC, PICC will be disabled" ;
      case 0xCA:  return "Previous command was not fully completed.\r\nNot all Frames were requested or provided by the PCD." ;
      case 0xCD:  return "PICC was disabled by an unrecoverable error" ;
      case 0xCE:  return "Number of applications limit reached" ;
      case 0xDE:  return "File/application with same number already exists" ;
      case 0xEE:  return "Could not complete NV-write operation due to loss of power" ;
      case 0xF0:  return "Specified file number does not exist" ;
      case 0xF1:  return "Unrecoverable error within file, file will be disabled" ;
      default:    return "Unknown error" ;
      } ;
   }



static const char* SC_GetLastError(void)
   {
   static char buf[256] ;

	switch (ErrorCode)
      {
      case 1:                                return "Invalid parameter." ;
      case 2:                                return "Result is too short.";
      case 3:
         sprintf_s(buf, sizeof(buf), "SC Error: 0x%04X\r\n%s", SW12, DecodeError(SW12)) ;
         return buf ;
      case 4:                                return "Unexpected result length." ;
      case 31:                               return "Device error" ;
		case ERROR_BROKEN_PIPE:					   return "Error Code - 0x00000109. The client attempted a smart card operation in a remote session, such as a client session running on a terminal server, and the operating system in use does not support smart card redirection.";
		case SCARD_E_BAD_SEEK:					   return "Error Code - 0x80100029. An error occurred in setting the smart card file object pointer." ;
		case SCARD_E_CANCELLED:					   return "Error Code - 0x80100002. The action was canceled by an SCardCancel request." ;
      case SCARD_E_CANT_DISPOSE:				   return "Error Code - 0x8010000E. The system could not dispose of the media in the requested manner." ;
		case SCARD_E_CARD_UNSUPPORTED:			return "Error Code - 0x8010001C. The smart card does not meet minimal requirements for support." ;
		case SCARD_E_CERTIFICATE_UNAVAILABLE:	return "Error Code - 0x8010002D. The requested certificate could not be obtained." ;
		case SCARD_E_COMM_DATA_LOST:			   return "Error Code - 0x8010002F. A communications error with the smart card has been detected." ;
		case SCARD_E_DIR_NOT_FOUND:				return "Error Code - 0x80100023. The specified directory does not exist in the smart card." ;
		case SCARD_E_DUPLICATE_READER:			return "Error Code - 0x8010001B. The reader driver did not produce a unique reader name." ;
		case SCARD_E_FILE_NOT_FOUND:			   return "Error Code - 0x80100024. The specified file does not exist in the smart card." ;
		case SCARD_E_ICC_CREATEORDER:			   return "Error Code - 0x80100021. The requested order of object creation is not supported." ;
		case SCARD_E_ICC_INSTALLATION:			return "Error Code - 0x80100020. No primary provider can be found for the smart card." ;
		case SCARD_E_INSUFFICIENT_BUFFER:		return "Error Code - 0x80100008. The data buffer for returned data is too small for the returned data." ;
		case SCARD_E_INVALID_ATR:				   return "Error Code - 0x80100015. An ATR string obtained from the registry is not a valid ATR string." ;
		case SCARD_E_INVALID_CHV:				   return "Error Code - 0x8010002A. The supplied PIN is incorrect." ;
		case SCARD_E_INVALID_HANDLE:			   return "Error Code - 0x80100003. The supplied handle was not valid." ;
		case SCARD_E_INVALID_PARAMETER:			return "Error Code - 0x80100004. One or more of the supplied parameters could not be properly interpreted." ;
		case SCARD_E_INVALID_TARGET:			   return "Error Code - 0x80100005. Registry startup information is missing or not valid." ;
		case SCARD_E_INVALID_VALUE:				return "Error Code - 0x80100011. One or more of the supplied parameter values could not be properly interpreted." ;
		case SCARD_E_NO_ACCESS:					   return "Error Code - 0x80100027. Access is denied to the file." ;
		case SCARD_E_NO_DIR:					      return "Error Code - 0x80100025. The supplied path does not represent a smart card directory." ;
		case SCARD_E_NO_FILE:					   return "Error Code - 0x80100026. The supplied path does not represent a smart card file." ;
		case SCARD_E_NO_KEY_CONTAINER:			return "Error Code - 0x80100030. The requested key container does not exist on the smart card." ;
		case SCARD_E_NO_MEMORY:					   return "Error Code - 0x80100006. Not enough memory available to complete this command." ;
		case SCARD_E_NO_PIN_CACHE:				   return "Error Code - 0x80100033. The smart card PIN cannot be cached. Windows Server 2008, Windows Vista, Windows Server 2003 and Windows XP : This error code is not available." ;
		case SCARD_E_NO_READERS_AVAILABLE:		return "Error Code - 0x8010002E. No smart card reader is available." ;
		case SCARD_E_NO_SERVICE:				   return "Error Code - 0x8010001D. The smart card resource manager is not running." ;
		case SCARD_E_NO_SMARTCARD:				   return "Error Code - 0x8010000C. The operation requires a smart card, but no smart card is currently in the device." ;
		case SCARD_E_NO_SUCH_CERTIFICATE:		return "Error Code - 0x8010002C. The requested certificate does not exist." ;
		case SCARD_E_NOT_READY:					   return "Error Code - 0x80100010. The reader or card is not ready to accept commands." ;
		case SCARD_E_NOT_TRANSACTED:			   return "Error Code - 0x80100016. An attempt was made to end a nonexistent transaction." ;
		case SCARD_E_PCI_TOO_SMALL:				return "Error Code - 0x80100019. The PCI receive buffer was too small." ;
		case SCARD_E_PIN_CACHE_EXPIRED:			return "Error Code - 0x80100032. The smart card PIN cache has expired. Windows Server 2008, Windows Vista, Windows Server 2003 and Windows XP : This error code is not available." ;
		case SCARD_E_PROTO_MISMATCH:			   return "Error Code - 0x8010000F. The requested protocols are incompatible with the protocol currently in use with the card." ;
		case SCARD_E_READ_ONLY_CARD:			   return "Error Code - 0x80100034. The smart card is read - only and cannot be written to. Windows Server 2008, Windows Vista, Windows Server 2003 and Windows XP : This error code is not available." ;
		case SCARD_E_READER_UNAVAILABLE:		   return "Error Code - 0x80100017. The specified reader is not currently available for use." ;
		case SCARD_E_READER_UNSUPPORTED:		   return "Error Code - 0x8010001A. The reader driver does not meet minimal requirements for support." ;
		case SCARD_E_SERVER_TOO_BUSY:			   return "Error Code - 0x80100031. The smart card resource manager is too busy to complete this operation." ;
		case SCARD_E_SERVICE_STOPPED:			   return "Error Code - 0x8010001E. The smart card resource manager has shut down." ;
		case SCARD_E_SHARING_VIOLATION:			return "Error Code - 0x8010000B. The smart card cannot be accessed because of other outstanding connections." ;
		case SCARD_E_SYSTEM_CANCELLED:			return "Error Code - 0x80100012. The action was canceled by the system, presumably to log off or shut down." ;
		case SCARD_E_TIMEOUT:					   return "Error Code - 0x8010000A. The user - specified time - out value has expired." ;
		case SCARD_E_UNEXPECTED:				   return "Error Code - 0x8010001F. An unexpected card error has occurred." ;
		case SCARD_E_UNKNOWN_CARD:				   return "Error Code - 0x8010000D. The specified smart card name is not recognized." ;
		case SCARD_E_UNKNOWN_READER:			   return "Error Code - 0x80100009. The specified reader name is not recognized." ;
		case SCARD_E_UNKNOWN_RES_MNG:			   return "Error Code - 0x8010002B. An unrecognized error code was returned." ;
		case SCARD_E_UNSUPPORTED_FEATURE:		return "Error Code - 0x80100022. This smart card does not support the requested feature." ;
		case SCARD_E_WRITE_TOO_MANY:			   return "Error Code - 0x80100028. An attempt was made to write more data than would fit in the target object." ;
		case SCARD_F_COMM_ERROR:				   return "Error Code - 0x80100013. An internal communications error has been detected." ;
		case SCARD_F_INTERNAL_ERROR:			   return "Error Code - 0x80100001. An internal consistency check failed." ;
		case SCARD_F_UNKNOWN_ERROR:				return "Error Code - 0x80100014. An internal error has been detected, but the source is unknown." ;
		case SCARD_F_WAITED_TOO_LONG:			   return "Error Code - 0x80100007. An internal consistency timer has expired." ;
		case SCARD_P_SHUTDOWN:					   return "Error Code - 0x80100018. The operation has been aborted to allow the server application to exit." ;
		case SCARD_S_SUCCESS:					   return "" ;
		case SCARD_W_CANCELLED_BY_USER:			return "Error Code - 0x8010006E. The action was canceled by the user." ;
		case SCARD_W_CACHE_ITEM_NOT_FOUND:		return "Error Code - 0x80100070. The requested item could not be found in the cache." ;
		case SCARD_W_CACHE_ITEM_STALE:			return "Error Code - 0x80100071. The requested cache item is too old and was deleted from the cache." ;
		case SCARD_W_CACHE_ITEM_TOO_BIG:		   return "Error Code - 0x80100072. The new cache item exceeds the maximum per - item size defined for the cache." ;
		case SCARD_W_CARD_NOT_AUTHENTICATED:	return "Error Code - 0x8010006F. No PIN was presented to the smart card." ;
		case SCARD_W_CHV_BLOCKED:				   return "Error Code - 0x8010006C. The card cannot be accessed because the maximum number of PIN entry attempts has been reached." ;
		case SCARD_W_EOF:						      return "Error Code - 0x8010006D. The end of the smart card file has been reached." ;
		case SCARD_W_REMOVED_CARD:				   return "Error Code - 0x80100069. The smart card has been removed, so further communication is not possible." ;
		case SCARD_W_RESET_CARD:				   return "Error Code - 0x80100068. The smart card was reset." ;
		case SCARD_W_SECURITY_VIOLATION:		   return "Error Code - 0x8010006A. Access was denied because of a security violation." ;
		case SCARD_W_UNPOWERED_CARD:			   return "Error Code - 0x80100067. Power has been removed from the smart card, so that further communication is not possible." ;
		case SCARD_W_UNRESPONSIVE_CARD:			return "Error Code - 0x80100066. The smart card is not responding to a reset." ;
		case SCARD_W_UNSUPPORTED_CARD:			return "Error Code - 0x80100065. The reader cannot communicate with the card, due to ATR string configuration conflicts." ;
		case SCARD_W_WRONG_CHV:					   return "Error Code - 0x8010006B. The card cannot be accessed because the wrong PIN was presented." ;
			
		default:
				return "Unknown Error Code" ;
	   } ;
   }
