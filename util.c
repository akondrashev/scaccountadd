#include "stdafx.h"


static WORD CRCTable[256] ;

static const BYTE reversed_bytes[] =
{
   0x00, 0x80, 0x40, 0xC0, 0x20, 0xA0, 0x60, 0xE0,
   0x10, 0x90, 0x50, 0xD0, 0x30, 0xB0, 0x70, 0xF0,
   0x08, 0x88, 0x48, 0xC8, 0x28, 0xA8, 0x68, 0xE8,
   0x18, 0x98, 0x58, 0xD8, 0x38, 0xB8, 0x78, 0xF8,
   0x04, 0x84, 0x44, 0xC4, 0x24, 0xA4, 0x64, 0xE4,
   0x14, 0x94, 0x54, 0xD4, 0x34, 0xB4, 0x74, 0xF4,
   0x0C, 0x8C, 0x4C, 0xCC, 0x2C, 0xAC, 0x6C, 0xEC,
   0x1C, 0x9C, 0x5C, 0xDC, 0x3C, 0xBC, 0x7C, 0xFC,
   0x02, 0x82, 0x42, 0xC2, 0x22, 0xA2, 0x62, 0xE2,
   0x12, 0x92, 0x52, 0xD2, 0x32, 0xB2, 0x72, 0xF2,
   0x0A, 0x8A, 0x4A, 0xCA, 0x2A, 0xAA, 0x6A, 0xEA,
   0x1A, 0x9A, 0x5A, 0xDA, 0x3A, 0xBA, 0x7A, 0xFA,
   0x06, 0x86, 0x46, 0xC6, 0x26, 0xA6, 0x66, 0xE6,
   0x16, 0x96, 0x56, 0xD6, 0x36, 0xB6, 0x76, 0xF6,
   0x0E, 0x8E, 0x4E, 0xCE, 0x2E, 0xAE, 0x6E, 0xEE,
   0x1E, 0x9E, 0x5E, 0xDE, 0x3E, 0xBE, 0x7E, 0xFE,
   0x01, 0x81, 0x41, 0xC1, 0x21, 0xA1, 0x61, 0xE1,
   0x11, 0x91, 0x51, 0xD1, 0x31, 0xB1, 0x71, 0xF1,
   0x09, 0x89, 0x49, 0xC9, 0x29, 0xA9, 0x69, 0xE9,
   0x19, 0x99, 0x59, 0xD9, 0x39, 0xB9, 0x79, 0xF9,
   0x05, 0x85, 0x45, 0xC5, 0x25, 0xA5, 0x65, 0xE5,
   0x15, 0x95, 0x55, 0xD5, 0x35, 0xB5, 0x75, 0xF5,
   0x0D, 0x8D, 0x4D, 0xCD, 0x2D, 0xAD, 0x6D, 0xED,
   0x1D, 0x9D, 0x5D, 0xDD, 0x3D, 0xBD, 0x7D, 0xFD,
   0x03, 0x83, 0x43, 0xC3, 0x23, 0xA3, 0x63, 0xE3,
   0x13, 0x93, 0x53, 0xD3, 0x33, 0xB3, 0x73, 0xF3,
   0x0B, 0x8B, 0x4B, 0xCB, 0x2B, 0xAB, 0x6B, 0xEB,
   0x1B, 0x9B, 0x5B, 0xDB, 0x3B, 0xBB, 0x7B, 0xFB,
   0x07, 0x87, 0x47, 0xC7, 0x27, 0xA7, 0x67, 0xE7,
   0x17, 0x97, 0x57, 0xD7, 0x37, 0xB7, 0x77, 0xF7,
   0x0F, 0x8F, 0x4F, 0xCF, 0x2F, 0xAF, 0x6F, 0xEF,
   0x1F, 0x9F, 0x5F, 0xDF, 0x3F, 0xBF, 0x7F, 0xFF
};


/*-----------------------------------------------\
|  Function:   reverse_array_bits                |
|  Purpose :   Reverse the ordering of all       |
|              of the bits in an array.          |
|              i.e. Swap bit 7 of the last byte  |
|                   with bit 0 of the first byte |
|  Comments:                                     |
\-----------------------------------------------*/
void reverse_array_bits(BYTE *d, BYTE *s, size_t n)
   {
   s += n - 1 ;
   while (n--)
      {
      *(d++) = reversed_bytes[*(s--)];
      } ;
   }

/**
 * Shifts all the bits in a string by a given number of bits
 * @param number     Number of bits to shift
 * @param data       Data to shift
 * @param n_size     Size of data to shift
 */
void ShiftBits(BYTE left_shift, BYTE *data, size_t n_size)
   {
   register BYTE  right_shift ;

   left_shift = left_shift & 0x7 ;
   if (left_shift == 0) return ;

   right_shift = 8 - left_shift ;
   while (--n_size)
      {
      register BYTE a = data[0] ;
      register BYTE b = data[1] ;
      *data++ = (a << left_shift) | (b >> right_shift) ;
      } ;

   *data <<= left_shift ;
   }


void GetWindowsError(DWORD errCode, char *errMsg, size_t len)
   {
   if (!errCode || !FormatMessage(FORMAT_MESSAGE_FROM_SYSTEM|FORMAT_MESSAGE_IGNORE_INSERTS, NULL, errCode, MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), errMsg, (DWORD)len, NULL)) strcpy_s(errMsg, (DWORD)len, "Unknown Windows error") ;
   }


void swabl(char *src, char *dest, size_t num)
	{
   register BYTE s1, s2, s3 ;

   num >>= 2 ;                                   /* long word count */
   while (num--)
      {
      s3 = *src++ ;
      s2 = *src++ ;
      s1 = *src++ ;
      *dest++ = *src ;
      *dest++ = s1 ;
      *dest++ = s2 ;
      *dest++ = s3 ;
      } ;
	}

WORD crc16(BYTE *ptr, size_t Count)
   {
	WORD crc = 0xffff ;

	while (Count--) crc = (crc >> 8) ^ CRCTable[*ptr++ ^ (unsigned char)crc] ;
	return crc;
   }


WORD crc16ex(WORD crc, BYTE *ptr, size_t Count)
   {
	while (Count--) crc = (crc >> 8) ^ CRCTable[*ptr++ ^ (unsigned char)crc] ;
	return crc;
   }


void crc_table(void)
   {
	WORD   z ;
   size_t i ;

	for (i = 0; i < 256; i++)
      {
		z = (WORD)((i >> 4) ^ (i & 0x0f)) ;
		z = (z >> 2) ^ (z & 0x03) ;
		z = ((z >> 1) ^ (z & 0x01)) ? 0xc001 : 0 ;
		CRCTable[i] = (WORD)(z ^ (i << 6) ^ (i << 7)) ;
	   } ;
   }


void FixCRC(BYTE *vector, size_t length)
   {
	WORD crc = crc16(vector, length - 2) ;
	vector[length - 2] = (BYTE)crc ;
	vector[length - 1] = (BYTE)(crc >> 8) ;
   }


BYTE Hex(char c)
   {
   if ('0' <= c && c <= '9') return (c - '0') ;
   if ('A' <= c && c <= 'F') return (c - 55) ;
   return 0 ;
   }


LONG64 A2L(char *lp)
   {
   LONG64 v = 0 ;
   size_t n = strlen(lp) ;

   if (n == 0) return v ;
   if (n > 2 && lp[0] == '0' && lp[1] == 'x')
      {
      lp += 2 ;
      n -= 2 ;
      do
         {
         v = (v << 4) | Hex((char)toupper(*lp++)) ;
         }
      while(--n) ;
      return v ;
      }
   else
      return _atoi64(lp) ;
   }


static const char HEX_TAB[] = "0123456789ABCDEF" ;

size_t HexToStr(unsigned char *lpHex, size_t len, char *lpBuf)
   {
   unsigned char c ;
   size_t   n = 0 ;

   while (len--)
      {
      c = *lpHex++ ;
      *lpBuf++ = HEX_TAB[c >> 4] ;
      *lpBuf++ = HEX_TAB[c & 0xF] ;
      n += 2 ;
      } ;
   *lpBuf = 0 ;
   return n ;
   }


size_t HexToStrC(unsigned char *lpHex, size_t len, char *lpBuf)
   {
   unsigned char c ;
   size_t   n = 0 ;

   while (len--)
      {
      c = *lpHex++ ;
      *lpBuf++ = '0' ;
      *lpBuf++ = 'x' ;
      *lpBuf++ = HEX_TAB[c >> 4] ;
      *lpBuf++ = HEX_TAB[c & 0xF] ;
      *lpBuf++ = ',' ;
      n += 5 ;
      } ;
   *--lpBuf = 0 ;
   return n - 1 ;
   }


void XorDataBlock(unsigned char *Out, const unsigned char *In, const unsigned char *Xor, size_t Length)
   {
   while (Length--) *Out++ = *In++ ^ *Xor++ ;
   }


// Random string generator
void CreateRnd(unsigned char *out, size_t len)
   {
   DWORD now = (DWORD)time(NULL) ^ GetTickCount() ;

   do
      {
      *out++ = (unsigned char)now ;
      now *= 127773;
      now += 16807;
      }
   while (--len) ;
   }


void RotateBlockLeft(unsigned char *Out, const unsigned char *In, size_t Length)
   {
   unsigned char last = *In++ ;
   
   while (--Length) *Out++ = *In++ ;
   *Out = last ;
   }


BOOLEAN IsAnNmber(char *s)
   {
   BOOLEAN rc = FALSE ;
   char c ;

   for (;;)
      {
      c = *s++ ;
      if (!c || c == ' ') return rc ;
      if (c < '0' || c > '9') return FALSE ;
      rc = TRUE ;
      } ;
   }


/*
static const char base64_chars[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/" ;

static BOOLEAN is_base64(unsigned char c)
   {
   return (isalnum(c) || (c == '+') || (c == '/')) ;
   }


static size_t find(const char *str, char c)
   {
   size_t  i = 0 ;
   char    t ;

   while (t = *str++)
      {
      if (t == c) return i ;
      i++ ;
      } ;
   return i ;
   }


size_t base64_decode(char *encoded_string, char *ret)
      {
		size_t in_len = strlen(encoded_string) ;
		int i = 0;
		int j = 0;
		int in_ = 0;
      size_t out = 0 ;
		unsigned char char_array_4[4], char_array_3[3];

		while (in_len-- && (encoded_string[in_] != '=') && is_base64(encoded_string[in_])) {
			char_array_4[i++] = encoded_string[in_]; in_++;
			if (i == 4) {
				for (i = 0; i < 4; i++)
					char_array_4[i] = (unsigned char)find(base64_chars, char_array_4[i]);

				char_array_3[0] = (char_array_4[0] << 2) + ((char_array_4[1] & 0x30) >> 4);
				char_array_3[1] = ((char_array_4[1] & 0xf) << 4) + ((char_array_4[2] & 0x3c) >> 2);
				char_array_3[2] = ((char_array_4[2] & 0x3) << 6) + char_array_4[3];

				for (i = 0; (i < 3); i++)
               {
					*ret++ = char_array_3[i];
               out++ ;
               } ;
				i = 0;
			}
		}

		if (i) {
			for (j = i; j < 4; j++)
				char_array_4[j] = 0;

			for (j = 0; j < 4; j++)
				char_array_4[j] = (unsigned char)find(base64_chars, char_array_4[j]);

			char_array_3[0] = (char_array_4[0] << 2) + ((char_array_4[1] & 0x30) >> 4);
			char_array_3[1] = ((char_array_4[1] & 0xf) << 4) + ((char_array_4[2] & 0x3c) >> 2);
			char_array_3[2] = ((char_array_4[2] & 0x3) << 6) + char_array_4[3];

			for (j = 0; (j < i - 1); j++)
            {
            *ret++ = char_array_3[j];
            out++ ;
            } ;
		}

	};
*/


DWORD Crc32(const BYTE *Data, size_t Length, DWORD Crc)
   {
   size_t  i ;
   BOOLEAN b ;

   while (Length--)
      {
      Crc ^= *Data++ ;
      for (i = 0; i < 8; i++)
         {
         b = Crc & 0x01 ;
         Crc >>= 1 ;
         if (b) Crc ^= 0xEDB88320 ;
         } ;
      } ;
   return Crc;
   }


WORD swabw(WORD w)
   {
   return (w << 8)|(w >> 8) ;
   }


DWORD bfx(const unsigned char *cptr, DWORD bit_offset, DWORD bit_len)
   {
   DWORD  c ;

   // Calculate byte offset(first byte=0) of start byte
   c = --bit_offset >> 3 ;
   // Calculate how many bits to shift bit field left to clear bits before bit field
   bit_offset -= c << 3 ;
   // Pointer to first byte to load (big endian order)
   cptr += c ;
   // Right shift
   bit_len = 32 - bit_len ;
   // Load 32 bits
   c = (((DWORD)*cptr) << 24) | (((DWORD)cptr[1]) << 16) | (((DWORD)cptr[2]) << 8) | ((DWORD)cptr[3]) ;
   // Extract
   return ((c << bit_offset) >> bit_len) ;
   }


void bfi(unsigned char *cptr, DWORD bit_offset, DWORD bit_len, DWORD value)
   {
   DWORD  c, left_margin, right_margin, mask ;

   // Calculate byte offset(first byte=0) of start byte
   c = --bit_offset >> 3 ;
   // Calculate how many unused bits on the left
   left_margin = bit_offset - (c << 3) ;
   // Address of first byte to load
   cptr += c ;
   // Calculate how many unused bits on the right
   right_margin = 32 - bit_len - left_margin ;
   // Create mask to clean bit field
   mask = (0xFFFFFFFF << (right_margin + left_margin)) >> left_margin ;
   // Load 32 bits
   c = (((DWORD)*cptr) << 24) | (((DWORD)cptr[1]) << 16) | (((DWORD)cptr[2]) << 8) | ((DWORD)cptr[3]) ;
   // Apply value
   c = (c & ~mask) | ((value << right_margin) & mask) ;
   // Write back
   *cptr = (BYTE)(c >> 24) ;
   cptr[1] = (BYTE)(c >> 16) ;
   cptr[2] = (BYTE)(c >> 8) ;
   cptr[3] = (BYTE)c ;
   }



static __inline int digit(char c)
   {
   if (c < '0' || c > '9') return -1 ;
   return (c - '0') ;
   }


int GetNum(char **pp)
   {
   char *p ;
   int   d, v ;
      
   p = *pp ;
   if (*p == ' ') p++ ;
   d = digit(*p++) ;
   if (d < 0) return -1 ;
   while (1)
      {
      v = digit(*p++) ;
      if (v < 0) break ;
      d = d*10 + v ;
      } ;
   *pp = --p ;
   return d ;
   }


char *FormatExpDate(DWORD d)
   {
   __time64_t t2 ;
   struct tm  tm ;
   static char date_buf[64] ;

   if (d == 0 || d == 0xFFFF) return "NO EXP" ;
   t2 = d * DaySec ;
   _gmtime64_s(&tm, &t2) ;
   sprintf_s(date_buf, sizeof(date_buf), "%02u/%02u/%04u", tm.tm_mon+1, tm.tm_mday, tm.tm_year+1900) ;
   return date_buf ;
   }


char *FormatDate(void)
   {
   __time64_t t ;
   struct tm tm ;
   static char date_buf[64] ;
   
   _time64(&t) ;
   _localtime64_s(&tm, &t) ;
   sprintf_s(date_buf, sizeof(date_buf), "%02u/%02u/%04u %02u:%02u:%02u", tm.tm_mon+1, tm.tm_mday, tm.tm_year+1900, tm.tm_hour, tm.tm_min, tm.tm_sec) ;
   return date_buf ;
   }
