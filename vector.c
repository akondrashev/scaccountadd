#include "vector.h"


void VD_Clear(VECTOR_DWORD *pVector)
   {
   pVector->numElements = 0 ;
   }


size_t VD_Append(VECTOR_DWORD *pVector, DWORD newElem)
   {
   pVector->vector[pVector->numElements++] = newElem ;
   return pVector->numElements ;
   }


BOOLEAN VD_ElemExists(DWORD newElem, VECTOR_DWORD *pVector)
   {
   size_t  i ;

   for (i = 0; i < pVector->numElements; i++) if (pVector->vector[i] == newElem) return TRUE ;
   return FALSE ;
   }


void VB_Clear(VECTOR_BYTE *pVector)
   {
   pVector->numElements = 0 ;
   }


size_t VB_Append(VECTOR_BYTE *pVector, BYTE newElem)
   {
   pVector->vector[pVector->numElements++] = newElem ;
   return pVector->numElements ;
   }


BOOLEAN VB_ElemExists(BYTE newElem, VECTOR_BYTE *pVector)
   {
   size_t  i ;

   for (i = 0; i < pVector->numElements; i++) if (pVector->vector[i] == newElem) return TRUE ;
   return FALSE ;
   }
