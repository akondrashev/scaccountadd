#include "stdafx.h"

char ReadDirectory[MAX_PATH] ;
const char *LogFileName = "SCAccountAdd.txt" ;
char FullLogFileName[MAX_PATH] ;

HANDLE  hLogFile ;


char* GetDirectoryFromPath(char *pDir, char *pFullPath)
   {
   size_t  nSize ;
   char   *p, c; 

   nSize = strlen(pFullPath) ;
   if (nSize)
      {
      p = pFullPath + nSize ;
      do
         {
         c = *--p ;
         if (c == 0 || c == '\\' || c == ':') break ;
         }
      while (--nSize) ;
      if (nSize)
         {
         memmove(pDir, pFullPath, nSize) ;
         pDir[nSize] = 0 ;
         return (p+1) ;
         } ;
      } ;
   *pDir = 0 ;
   return pFullPath ;
   }



void GetDirectory(void)
   {
   DWORD   nSize ;

   nSize = GetModuleFileName(NULL, ReadDirectory, sizeof(ReadDirectory)-1) ;
   if (nSize)
      GetDirectoryFromPath(ReadDirectory, ReadDirectory) ;
   else
      strcpy_s(ReadDirectory, sizeof(ReadDirectory), "C:\\") ;
   }


BOOLEAN CreateFileNames(void)
   {
   DWORD err ;

   GetDirectory() ;
   strcpy_s(FullLogFileName, sizeof(FullLogFileName), ReadDirectory) ;
   strcat_s(FullLogFileName, sizeof(FullLogFileName), LogFileName) ;
   hLogFile = CreateFile(FullLogFileName, GENERIC_READ|GENERIC_WRITE, FILE_SHARE_READ, NULL, OPEN_ALWAYS, FILE_FLAG_WRITE_THROUGH, NULL) ;
   if (hLogFile == INVALID_HANDLE_VALUE)
      {
      err = GetLastError() ;
      return FALSE ;
      } ;
   return TRUE ;
   }


#if 0
BOOLEAN ReadFlatCardFile(DWORD CardType)
   {
   char  *name, *p, c ;
   BYTE  *s ;
   char   buf[256] ;
   size_t nSize, j ;
   char   section[16] ;
   int    i ;

   DestroyContols() ;
   memset(&Card, 0, sizeof(Card)) ;
   memset(&FlatCard, 0, sizeof(FlatCard)) ;
   ApplicationRead = (Application)0 ;

   if (CardType == CARD_TYPE_CLASSIC)
      {
      name = "CLASSIC" ;
      nSize = 0 ;
      nSize += GetPrivateProfileString(name, "Page0", NULL, &buf[nSize], sizeof(buf) - 1 - (DWORD)nSize, FullCardName) ;
      if (nSize != 32) return false ;
      nSize += GetPrivateProfileString(name, "Page1", NULL, &buf[nSize], sizeof(buf) - 1 - (DWORD)nSize, FullCardName) ;
      if (nSize != 64) return false ;
      nSize += GetPrivateProfileString(name, "Page2", NULL, &buf[nSize], sizeof(buf) - 1 - (DWORD)nSize, FullCardName) ;
      if (nSize != 96) return false ;
      }
   else if (CardType == CARD_TYPE_TRIM)
      {
      name = "MAGNETIC" ;
      nSize = 0 ;
      for (i = 0; i < 10; i++)
         {
         sprintf_s(section, sizeof(section), "Page%u", i) ;
         nSize += GetPrivateProfileString(name, section, NULL, &buf[nSize], sizeof(buf) - 1 - (DWORD)nSize, FullCardName) ;
         } ;
      if (nSize != 80) return false ;
      }
   else
      return false ;
   
   nSize >>= 1 ;
   p = buf ;
   s = FlatCard.new_data ;
   for (j = 0; j < nSize; j++)
      {
      c = (char)toupper(*p++) ;
      *s++ = (Hex(c) << 4) | Hex((char)toupper(*p++)) ;
      } ;

   return true ;
   }


BOOLEAN ReadConfigFile(HWND hWnd, CARD_PARAMS *pParams)
   {
   int     rc ;

   rc = GetPrivateProfileInt("Account", "AgencyID", -1, FullIniFileName) ;
   if (rc < 1 || rc > AGENCY_GFI)
      {
      ErrorBox(hWnd, "Invalid Agency ID in configuration file") ;
      return false ;
      } ;
   pParams->AgencyID = (iAgency)rc ;

   rc = GetPrivateProfileInt("Account", "ProductID", -1, FullIniFileName) ;
   if (rc < 0 || rc > 7)
      {
      ErrorBox(hWnd, "Invalid Product ID in configuration file") ;
      return false ;
      } ;
   pParams->ProductType = rc ;

   rc = GetPrivateProfileInt("Account", "Designator", -1, FullIniFileName) ;
   if (rc < 0 || rc > 240)
      {
      ErrorBox(hWnd, "Invalid Designator in configuration file") ;
      return false ;
      } ;
   pParams->ProductDesig = rc ;

   rc = GetPrivateProfileInt("Account", "Active", -1, FullIniFileName) ;
   if (rc < 0 || rc > 1)
      {
      ErrorBox(hWnd, "Invalid Account Status in configuration file") ;
      return false ;
      } ;
   pParams->AccStatus = rc ;

   return true ;
   }
#endif



DWORD WriteOutFile(HANDLE hFile, char *fmt, ...)
   {
   char     *p ;
   static char buf[512] ;
   int      len ;
   va_list  my_list ;
   DWORD    nBytes ;

   if (hFile == INVALID_HANDLE_VALUE) return 1 ;
   SetFilePointer(hFile, 0, NULL, FILE_END) ;
   va_start(my_list, fmt) ;
   len = vsprintf_s(buf, sizeof(buf), fmt, my_list) ;
   va_end(my_list) ;
   p = buf ;
   while (len)
      {
      if (!WriteFile(hFile, buf, len, &nBytes, NULL))
         {
         return GetLastError() ;
         } ;
      p += nBytes ;
      len -= nBytes ;
      } ;
   return 0 ;
   }

