#ifndef _UTIL_H_
#define _UTIL_H_

#include <Windows.h>

void GetWindowsError(DWORD errCode, char *errMsg, size_t len) ;
void swabl(char *src, char *dest, size_t num) ;
void reverse_array_bits(BYTE *d, BYTE *s, size_t n) ;
void ShiftBits(BYTE number, BYTE *data, size_t n_size) ;

WORD   crc16(BYTE *ptr, size_t Count) ;
WORD   crc16ex(WORD crc, BYTE *ptr, size_t Count) ;
void   crc_table(void) ;
void   FixCRC(BYTE *vector, size_t length) ;
LONG64 A2L(char *lp) ;
BYTE   Hex(char c) ;
size_t HexToStr(unsigned char *lpHex, size_t len, char *lpBuf) ;
size_t HexToStrC(unsigned char *lpHex, size_t len, char *lpBuf) ;
void   XorDataBlock(unsigned char *Out, const unsigned char *In, const unsigned char *Xor, size_t Length) ;
void   CreateRnd(unsigned char *out, size_t len) ;
void   RotateBlockLeft(unsigned char *Out, const unsigned char *In, size_t Length) ;
BOOLEAN IsAnNmber(char *s) ;
DWORD  Crc32(const BYTE *Data, size_t Length, DWORD Crc) ;
WORD   swabw(WORD w) ;

DWORD bfx(const unsigned char *cptr, DWORD bit_offset, DWORD bit_len) ;
void  bfi(unsigned char *cptr, DWORD bit_offset, DWORD bit_len, DWORD value) ;

int GetNum(char **pp) ;

#endif
