#ifndef _DESFIRECARD_H_
#define _DESFIRECARD_H_

typedef struct
   {
   DWORD format_version;
   BYTE  ascii_digit[16];
   BYTE  terminator ;
   DWORD frendly_agcy;
   DWORD crc;
   } PRINTED_ID_T ;


typedef struct
   {
   DWORD  format_version ;
   DWORD  user_profile  ;
   DWORD  card_exp_date ;
   DWORD  profile_exp_date ;
   DWORD  agency_id ;
   DWORD  issue_number ;
   DWORD  tpb_lock ;
   DWORD  tpb_code ;
   DWORD  security_code ;
   DWORD  test_card ;
   DWORD  maintenance_use ;
   DWORD  mfg_id ;
   DWORD  equipment_type ;
   DWORD  equipment_id ;
   DWORD  number_transfers ;
   DWORD  number_products ;
   DWORD  number_purses ;
   DWORD  number_paygo ;
   DWORD  number_bonus ;
   DWORD  crc ;
   } DES_CARD_PROP ;



typedef struct
   {
   DWORD  product_prior_cond ;
   DWORD  product_type ;
   } DES_PRODUCT_ELEMENTS ;


typedef struct
   {
   DWORD  format_version ;
   DWORD  purse_status[4] ;
   DES_PRODUCT_ELEMENTS  prod[10] ;
   DWORD  loyalty_counter ;
   DWORD  ind_load_seq_num ;
   DWORD  range_load_seq_num ;
   DWORD  transfer_status ;
   DWORD  card_registered ;
   DWORD  card_bad_listed ;
   DWORD  negative_allowed ;
   DWORD  paygo_status ;
   DWORD  bonus_status ;
   DWORD  accessibility ;
   DWORD  language_preference ;
   DWORD  crc ;
   } DES_PRODUCT_LIST ;


typedef struct
   {
   DWORD  format_version ;
   DWORD  designator ;
   DWORD  source_zone ;
   DWORD  destination_zone ;
   DWORD  expiration ;
   DWORD  orig_route ;
   DWORD  orig_dir ;
   DWORD  product_id ;
   DWORD  orig_fare ;
   DWORD  latest_fare ;
   DWORD  latest_route ;
   DWORD  latest_dir ;
   DWORD  transfer_trips ;
   DWORD  start_time ;
   DWORD  crc ;
   } DES_TRANS ;



typedef struct
   {
   DWORD  orig_addTime ;
   DWORD  type_exp ;
   DWORD  addTime ;
   DWORD  negative ;
   DWORD  pendingrecharges ;
   DWORD  days ;
   } DES_PERIOD_PASS ;

typedef struct
   {
   DWORD  remaining_rides ;
   DWORD  negative ;

   DWORD  recharge_rides ;
   DWORD  threshold ;
   } DES_STORED_RIDE ;

typedef struct
   {
   DWORD  remaining_value ;
   DWORD  paygo_end ;
   DWORD  bonus_ride_cnt ;
   DWORD  negative ;
   DWORD  paygo_ride_cnt ;

   DWORD  ReplenishAmount ;
   DWORD  threshold ;
   } DES_STORED_VALUE ;

typedef struct
   {
   DWORD  employeeID  ;
   DWORD  type_exp ;
   DWORD  addTime  ;
   } DES_MAINT ;


typedef struct
   {
   DWORD  format_version ;
   DWORD  product_type ;
   DWORD  designator ;
   DWORD  designator_details ;
   DWORD  start_date ;
   DWORD  exp_date ;
   DWORD  source_zone ;
   DWORD  destination_zone ;
   DWORD  mfgID ;
   DWORD  equipment_type ;
   DWORD  equipmentID ;
   DWORD  load_seq_number ;
   DWORD  registered ;
   DWORD  tpb_lock ;
   DWORD  auto_recharge ;
   DWORD  profile_link ;  /*currently user date linked to product*/
   DWORD  tpb_code ;
   DWORD  range_seq_num ;
   DWORD  crc ;
   DWORD  prodNum ;
   union
      {
      DES_PERIOD_PASS   pp;
      DES_STORED_RIDE   sr;
      DES_STORED_VALUE  sv;
      DES_MAINT         mn;
      } ;
   BYTE  ascii_digit[16];
   BYTE  terminator ;
   DWORD frendly_agcy;
   } DES_PROD_FILE ;




typedef struct
   {
   DWORD  charge ;
   DWORD  first_use_flag_v1 ;
   DWORD  fare_set ;
   DWORD  group ;
   DWORD  desig ;
   } DS_PERIOD_JOURNAL ;

typedef struct
   {
   DWORD  charge ;
   DWORD  balance ;
   DWORD  first_use_flag_v1 ;
   DWORD  fare_set ;
   DWORD  group ;
   DWORD  desig ;
   } DS_RIDE_JOURNAL ;

typedef struct
   {
   DWORD  deduction ;
   DWORD  balance ;
   DWORD  first_use_flag_v1 ;
   DWORD  fare_set ;
   DWORD  group ;
   DWORD  desig ;
   } DS_VALUE_JOURNAL ;

typedef struct
   {
   DWORD  deduction  ;
   DWORD  balance ;
   DWORD  group ;
   } DS_PURSE_JOURNAL ;

typedef struct
   {
   DWORD  charge ;
   DWORD  transfer_trips ;
   DWORD  first_use_flag_v1 ;
   DWORD  fare_set ;
   DWORD  group ;
   DWORD  desig ;
   } DS_TRANSFER_JOURNAL ;

typedef struct
   {
   DWORD  range_flag ;
   DWORD  pending_periods ;
   DWORD  fulfillment_rc ;
   DWORD  sequence_number ;
   DWORD  pkg_ID ;
   DWORD  val_before_al;
   DWORD  val_after_al;
   } DS_AUTO_JOURNAL ;


typedef struct
   {
   DWORD equipmentID ;
   DWORD timestamp ;
   DWORD format_version ;
   DWORD pkg_prod_id ;
   DWORD product_id ;
   DWORD transaction_type ;
   DWORD mfgID ;
   DWORD equipment_type ;
   DWORD crc ;
   DWORD group ;                 // Calculated field
   union
      {
      DS_PERIOD_JOURNAL      pj;
      DS_RIDE_JOURNAL        rj;
      DS_VALUE_JOURNAL       vj;
      DS_PURSE_JOURNAL       cj;
      DS_TRANSFER_JOURNAL    tj;
      DS_AUTO_JOURNAL        aj;
      } ;
   } DES_JOURNAL ;


typedef struct
   {
   DWORD format_version ;
   DWORD enabled ;
   DWORD badlisted ;
   DWORD load_seq_num ;
   DWORD group ;
   DWORD designator ;
   DWORD profile ;
   DWORD card_expiration ;
   DWORD crc ;
   } DES_ACCOUNT ;


typedef struct
   {
   DWORD  crc ;
   } DES_FILE_SPARE ;


#endif