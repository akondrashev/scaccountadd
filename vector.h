#ifndef _vector_h_
#define _vector_h_

#include <Windows.h>

#define MAX_VECTOR_SIZE    32

typedef struct
   {
   size_t   numElements ;
   DWORD    vector[MAX_VECTOR_SIZE] ;
   } VECTOR_DWORD ;


typedef struct
   {
   size_t   numElements ;
   BYTE     vector[MAX_VECTOR_SIZE] ;
   } VECTOR_BYTE ;


void    VD_Clear(VECTOR_DWORD *pVector) ;
size_t  VD_Append(VECTOR_DWORD *pVector, DWORD newElem) ;
BOOLEAN VD_ElemExists(DWORD newElem, VECTOR_DWORD *pVector) ;

void    VB_Clear(VECTOR_BYTE *pVector) ;
size_t  VB_Append(VECTOR_BYTE *pVector, BYTE newElem) ;
BOOLEAN VB_ElemExists(BYTE newElem, VECTOR_BYTE *pVector) ;



#endif