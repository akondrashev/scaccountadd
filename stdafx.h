// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently, but
// are changed infrequently
//

#pragma once

#include "targetver.h"

#define WIN32_LEAN_AND_MEAN             // Exclude rarely-used stuff from Windows headers
// Windows Header Files:
#include <windows.h>
#include <WindowsX.h>

// C RunTime Header Files
#include <stdlib.h>
#include <stdio.h>
#include <tchar.h>
#include <wchar.h>
#include <malloc.h>
#include <memory.h>
#include <time.h>
#include <CommDlg.h>

#define false     FALSE
#define true      TRUE

#include "vector.h"
#include "util.h"
#include "Hardware.h"
#include "DESFireCard.h"
#include "AgencyKeys.h"
#include "DESFireCommands.h"
#include "Controls.h"
//#include "FlatCards.h"
#include "Resource.h"

#include "SCAccountAdd.h"

// TODO: reference additional headers your program requires here
