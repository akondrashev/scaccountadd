#ifndef _DESFireCommands_h
#define _DESFireCommands_h

#define ADDITIONAL_DATA_FRAME_TO_BE_SEND   0x91AF

typedef enum
   {
	ReadData = (unsigned char)0xBD,
	ReadRecord = (unsigned char)0xBB
	} ReadMode ;


typedef enum
   {
	WriteData = (unsigned char)0x3D,
	WriteRecord = (unsigned char)0x3B
	} WriteMode ;


// Key Settings
typedef enum
   {
	/// 0x0B
	/// 1011
	/// ||||_____	PICC Master key is changeable (authentication with the current PICC master key necessary,	default setting)   
	/// |||______	GetApplicationIDs and GetKeySettings commands succeed independently of a preceding PICC master key authentication(default setting)
	/// ||_______	CreateApplication / DeleteApplication is permitted only with PICC master key authentication
	/// |________	Configuration chnagable with master PICC key 
	/// </Comment>
	KS_ALLOW_CHANGE_MK = 0x01,
	KS_FREE_LISTING_WITHOUT_MK = 0x02,
	KS_FREE_CREATE_DELETE_WITHOUT_MK = 0x04,
	KS_CONFIGURATION_CHANGEABLE = 0x08,
	KS_CHANGE_KEY_WITH_MK = 0x00,
	KS_CHANGE_KEY_WITH_TARGETED_KEYNO = 0xE0,
	KS_CHANGE_KEY_FROZEN = 0xF0,
	KS_DEFAULT = 0x0B
	} DESFireKeySettings ;


LONG DES_getApplicationIDs(HARDWARE *h, VECTOR_DWORD *app_list) ;
LONG DES_selectApplication(HARDWARE *h, Application App) ;
LONG DES_readData(HARDWARE *h, BYTE FileNo, unsigned int Idx, unsigned int len, unsigned char *File) ;
LONG DES_readRecord(HARDWARE *h, BYTE FileNo, unsigned int RecNum, unsigned char *File, unsigned int len) ;
LONG DES_getFileIDs(HARDWARE *h, VECTOR_BYTE *pFileIDs) ;
LONG DES_Authenticate(HARDWARE *h, KEY *pKey) ;
LONG DES_getValue(HARDWARE *h, BYTE FileNo, int *pValue) ;
LONG DES_deleteApplication(HARDWARE *h, Application App) ;
LONG DES_formatPICC(HARDWARE *h) ;
LONG DES_changeKey(HARDWARE *h, KEY *pNewKey, KEY *pKey, BOOLEAN SameKey) ;
LONG DES_createApplication(HARDWARE *h, Application ApplicationID, BYTE NoAppKeys, CryptoMode cm) ;
LONG DES_createBackupFile(HARDWARE *h, BYTE FileNo, DESFireFileEncryption CommSettings, unsigned short AccessRights, int FileSize) ;
LONG DES_createGFIBackupFile(HARDWARE *h, BYTE FileNo, Application App) ;
LONG DES_createStandardDataFile(HARDWARE *h, BYTE FileNo, DESFireFileEncryption CommSettings, unsigned short AccessRights, int FileSize) ;
LONG DES_createGFIStandardFile(HARDWARE *h, BYTE FileNo, Application App) ;
LONG DES_createCyclicRecordFile(HARDWARE *h, BYTE FileNo, DESFireFileEncryption CommSettings, unsigned short AccessRights, unsigned int RecordSize, unsigned int  MaxRecords) ;
LONG DES_createGFICyclicFile(HARDWARE *h, BYTE FileNo, Application App) ;
LONG DES_createValueFile(HARDWARE *h, BYTE FileNo, DESFireFileEncryption CommSettings, unsigned short AccessRights, int LowerLimit, int UpperLimit, int Value, unsigned char LimitedCreditEnabled) ;
LONG DES_createGFIValueFile(HARDWARE *h, BYTE FileNo, Application App) ;
LONG DES_commit(HARDWARE *h) ;
LONG DES_Debit(HARDWARE *h, BYTE FileNo, unsigned int Value) ;
LONG DES_Credit(HARDWARE *h, BYTE FileNo, unsigned int Value) ;
LONG DES_getValue(HARDWARE *h, BYTE FileNo, int *Value) ;
void DES_setCommit(BOOLEAN b) ;
LONG DES_write(HARDWARE *h, BYTE FileNo, BYTE *data, size_t len, DWORD SegmentOffset, WriteMode WM) ;
LONG DES_writeRecord(HARDWARE *h, BYTE FileNo, BYTE *data, size_t len) ;
LONG DES_writeData(HARDWARE *h, BYTE FileNo, BYTE *data, size_t len) ;
LONG DES_fileInfo(HARDWARE *h, BYTE FileNo, FILE_DESCR *pFile) ;

const char* DES_ErrorCode(uint16_t sw1sw2) ;


iAgency DES_GetAgencyIDFromApp(HARDWARE *h, Application app) ;

#endif