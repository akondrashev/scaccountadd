// SCAccountAdd.cpp : Defines the entry point for the application.
//

#include "stdafx.h"

#define MAX_LOADSTRING 100
#define IDB_READ       100
#define TIMER_ID       20

// Global Variables:
HINSTANCE  hInst ;								      // current instance
HWND       hwndMain ;
HANDLE     hCommThread ;
//HWND       hwndCreate ;
HFONT      hTextFont = NULL ;
HARDWARE  *h = NULL ;                          // Working hardware interface pointer
volatile BOOLEAN run ;
CARD_PARAMS  CardParams ;

TCHAR szTitle[MAX_LOADSTRING];					// The title bar text
TCHAR szWindowClass[MAX_LOADSTRING];			// the main window class name

const char *ReaderName1 = "ACS ACR1252 Dual Reader PICC 0" ;
const char *ReaderName2 = "ACS ACR1252 1S CL Reader PICC 0" ;

// Forward declarations of functions included in this code module:
BOOL	InitInstance(HINSTANCE, int);
LRESULT CALLBACK	WndProc(HWND, UINT, WPARAM, LPARAM);
INT_PTR CALLBACK	About(HWND, UINT, WPARAM, LPARAM);

static VECTOR_DWORD create_app_list, defined_app_list ;
static HCURSOR WaitCursor ;
static PROCESS_STATE CardState = CARD_STATE_NO ;
static DWORD  devNum ;
static LPTSTR lpCmd ;
static int   ww, wh, wx, wy, ws ;


const char *const state_txt[] = {"CREATED","EXISTS","ERROR"} ;


HWND MyRegisterClass(HINSTANCE hInstance, int nCmdShow)
   {
	WNDCLASSEX wcex ;
   HWND       hWnd ;

	wcex.cbSize        = sizeof(WNDCLASSEX);
	wcex.style			 = CS_HREDRAW | CS_VREDRAW;
	wcex.lpfnWndProc	 = WndProc;
	wcex.cbClsExtra	 = 0;
	wcex.cbWndExtra	 = 0;
	wcex.hInstance		 = hInstance;
	wcex.hIcon			 = LoadIcon(hInstance, MAKEINTRESOURCE(IDI_SCACCOUNTADD));
	wcex.hCursor		 = LoadCursor(NULL, IDC_ARROW);
	wcex.hbrBackground = (HBRUSH)(COLOR_BTNFACE+1);
	wcex.lpszMenuName	 = MAKEINTRESOURCE(IDC_SCACCOUNTADD);
	wcex.lpszClassName = szWindowClass;
	wcex.hIconSm		 = LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_SMALL));
   if (0 == RegisterClassEx(&wcex)) return NULL ;

//   hComboFont = CreateFont(12, 0, 0, 0, 400, 0, 0, 0, ANSI_CHARSET, OUT_OUTLINE_PRECIS, CLIP_DEFAULT_PRECIS, CLEARTYPE_QUALITY, FF_SWISS, "MS Shell Dlg 2") ;
   hTextFont = CreateFont(30, 0, 0, 0, 700, 0, 0, 0, ANSI_CHARSET, OUT_OUTLINE_PRECIS, CLIP_DEFAULT_PRECIS, CLEARTYPE_QUALITY, FF_SWISS, "Arial") ;

   ww = MAIN_WND_WIDTH + (GetSystemMetrics(SM_CXBORDER) << 1) ;
   wh = MAIN_WND_HEIGTH + (GetSystemMetrics(SM_CYBORDER) << 1) + GetSystemMetrics(SM_CYCAPTION) + GetSystemMetrics(SM_CYMENU) ;
   wx = GetSystemMetrics(SM_CXFULLSCREEN) - ww ;
   if (wx < 0)
      wx = 0 ;
   else
      wx = wx >> 1 ;
   wy = GetSystemMetrics(SM_CYFULLSCREEN) - wh ;
   if (wy < 0)
      wy = 0 ;
   else
      wy = wy >> 2 ;

   hWnd = CreateWindowEx(/*WS_EX_APPWINDOW|WS_EX_CLIENTEDGE|WS_EX_DLGMODALFRAME|WS_EX_STATICEDGE|*/WS_EX_TOPMOST, szWindowClass, szTitle, /*WS_POPUPWINDOW|*/WS_CAPTION|WS_CLIPCHILDREN/*|WS_MAXIMIZE|WS_MAXIMIZEBOX*/|WS_SYSMENU, 0, 0, GetSystemMetrics(SM_CXSCREEN), GetSystemMetrics(SM_CYSCREEN), NULL, NULL, hInstance, NULL) ;
   if (!hWnd) return FALSE ;

   //SetWindowPos(hWnd, HWND_TOP, 0, 0, GetSystemMetrics(SM_CXSCREEN), GetSystemMetrics(SM_CYSCREEN), SWP_SHOWWINDOW) ;
   ShowWindow(hWnd, SW_MAXIMIZE) ;
   ws = 1 ;
   UpdateWindow(hWnd) ;
   return hWnd ;
   }


int APIENTRY _tWinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPTSTR lpCmdLine, int nCmdShow)
   {
	MSG  msg ;

   hInst = hInstance ;
   lpCmd = lpCmdLine ;
   SetPriorityClass(GetCurrentProcess(), REALTIME_PRIORITY_CLASS) ;
   SetThreadPriority(GetCurrentThread(), THREAD_PRIORITY_TIME_CRITICAL) ;

	// Initialize global strings
	LoadString(hInstance, IDS_APP_TITLE, szTitle, MAX_LOADSTRING) ;
	LoadString(hInstance, IDC_SCACCOUNTADD, szWindowClass, MAX_LOADSTRING) ;
   crc_table() ;

   // Get hardware interface pointers
   h = GetPCSCIntrface() ;
	
   // Create main window
   if (!(hwndMain = MyRegisterClass(hInstance, nCmdShow))) return 1 ;
   // Thread ready event
   hCommThread = NULL ;

   WaitCursor = LoadCursor(NULL, IDC_WAIT) ;

	// Main message loop:
	while (GetMessage(&msg, NULL, 0, 0))
	   {
		TranslateMessage(&msg) ;
		DispatchMessage(&msg) ;
		} ;

   h->FreeDevList() ;
   if (hTextFont) DeleteObject(hTextFont) ;
   if (hLogFile != INVALID_HANDLE_VALUE) CloseHandle(hLogFile) ;
   return (int)msg.wParam ;
   }


void ErrorBox(HWND hwnd, const char *fmt, ...)
   {
   static char    buf[512], buf2[512] ;
   va_list my_list ;
   int     len ;

   if (fmt)
      {
      va_start(my_list, fmt) ;
      len = vsprintf_s(buf, sizeof(buf), fmt, my_list) ;
      va_end(my_list) ;
      if (len)
         {
         sprintf_s(buf2, sizeof(buf2), "%s\n%s", buf, h->GetLastSCError()) ;
         MessageBox(hwnd, buf2, "SC Add Account", MB_OK|MB_ICONSTOP) ;
         return ;
         } ;
      } ;
   MessageBox(hwnd, h->GetLastSCError(), "SC Editor", MB_OK|MB_ICONSTOP) ;
   }


static void StopCommThread(void)
   {
   if (hCommThread == NULL) return ;
   run = FALSE ;
   WaitForSingleObject(hCommThread, INFINITE) ;
   hCommThread = NULL ;
   }


int Hex2Bin(char c)
   {
   if ('0' <= c && c <= '9') return (c - '0') ;
   if ('A' <= c && c <= 'F') return (c - 55) ;
   return -1 ;
   }


static BOOL ParseCmdParam(HWND hWnd, LPTSTR lpCmd, CARD_PARAMS *lpParam)
   {
   int     v, u ;
   size_t  i, tot_len, len ;
   char    buf[32], str[32], key ;

   tot_len = strlen(lpCmd) ;
   len = tot_len >> 1 ;
   if (tot_len > 32 || tot_len != len*2)
      {
      ErrorBox(hWnd, "Invalid parameters string") ;
      return false ;
      } ;

   for (i = 0; i < len; i++)
      {
      v = Hex2Bin(*lpCmd++) ;
      if (v < 0)
         {
         ErrorBox(hWnd, "Invalid parameters string") ;
         return false ;
         } ;
      u = Hex2Bin(*lpCmd++) ;
      if (u < 0)
         {
         ErrorBox(hWnd, "Invalid parameters string") ;
         return false ;
         } ;
      buf[i] = (char)((v << 4) | u) ;
      } ;

   key = buf[0] ;
   for (i = 1; i < len; i++)
      {
      str[i-1] = buf[i] ^ key ^ (BYTE)(0x40 + i) ;
      } ;
   str[i-1] = 0 ;
   lpCmd = str ;

   v = GetNum(&lpCmd) ;
   if (v < 0) return false ;
   if (v < 1 || v > AGENCY_GFI)
      {
      ErrorBox(hWnd, "Invalid Agency ID specified") ;
      return false ;
      } ;
   lpParam->AgencyID = v ;

   v = GetNum(&lpCmd) ;
   if (v < 0 || v > 7)
      {
      ErrorBox(hWnd, "Invalid Product ID specified") ;
      return false ;
      } ;
   lpParam->ProductType = v ;

   v = GetNum(&lpCmd) ;
   if (v < 0 || v > 248)
      {
      ErrorBox(hWnd, "Invalid Product Designator specified") ;
      return false ;
      } ;
   lpParam->ProductDesig = v ;

   v = GetNum(&lpCmd) ;
   if (v < 0 || v > 1)
      {
      ErrorBox(hWnd, "Invalid Account Status specified") ;
      return false ;
      } ;
   lpParam->AccStatus = v ;

   return true ;
   }


static LONG InitApp(HWND hWnd)
   {
   DWORD  nUSBReaders, i ;
   char  *lpList ;

   if (!CreateFileNames())
      {
      ErrorBox(hWnd, "Error creating log file") ;
      return 1 ;
      } ;

   memset(&CardParams, 0, sizeof(CardParams)) ;
   if (!ParseCmdParam(hWnd, lpCmd, &CardParams))
      {
      ErrorBox(hWnd, "Parameters invalid") ;
      return 1 ;
      } ;

   // Get list of available readers
   lpList = h->GetDevList(&nUSBReaders) ;
   if (nUSBReaders == 0)
      {
      ErrorBox(hWnd, "SC reader not found") ;
      return 1 ;
      } ;
   for (i = 0; i < nUSBReaders; i++)
      {
      if (0 == strcmp(lpList, ReaderName1) || 0 == strcmp(lpList, ReaderName2))
         {
         devNum = i ;
         hCommThread = h->IsCardPresent(i, hWnd) ;
         if (hCommThread == NULL)
            {
            ErrorBox(hWnd, "Could not connect to SC reader") ;
            return 2 ;
            } ;
         break ;
         } ;
      lpList += 1 + strlen(lpList) ;
      } ;
   if (i == nUSBReaders)
      {
      ErrorBox(hWnd, "Supported SC reader not found") ;
      return 3 ;
      } ;
   return 0 ;
   }


//  Processes messages for the main window.
LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
   {
	int wmId, wmEvent, x, y ;
	PAINTSTRUCT ps;
   HCURSOR oldCursor ;
   RECT   rect ;
	HDC    hdc;
   char   buf[64] ;

	switch (message)
      {
      case WM_CREATE:
//         GetClientRect(hWnd, &rect) ;
//         hwndCreate = CreateWindow("BUTTON", "Create", WS_VISIBLE|WS_CHILD|WS_DISABLED, ((rect.right - rect.top) >> 1)-36, rect.bottom-45, 72, 25, hWnd, (HMENU)IDB_READ, hInst, NULL) ;
//         SendMessage(hwndCreate, WM_SETFONT, (WPARAM)GetStockObject(DEFAULT_GUI_FONT), 0) ;
         PostMessage(hWnd, WM_SC_INIT, 0, 0) ;
         break ;

      case WM_SC_INIT:
         if (InitApp(hWnd)) PostMessage(hWnd, WM_DESTROY, 0, 0) ;
         return 0 ;

      case WM_COMMAND:
         wmId    = LOWORD(wParam);
         wmEvent = HIWORD(wParam);
         // Parse the menu selections:
         switch (wmId)
            {
            case IDM_INFO:
               sprintf_s(buf, sizeof(buf), "Agency: %u, Product: %u, Designator: %u, Active: %u", CardParams.AgencyID, CardParams.ProductType, CardParams.ProductDesig, CardParams.AccStatus) ;
               MessageBox(hWnd, buf, "SC Add Account", MB_OK|MB_ICONINFORMATION) ;
               return 0 ;
            case IDM_ABOUT:
               sprintf_s(buf, sizeof(buf), "%s, Version %u.%u\r\nCopyright (C) 2020 SPX/Genfare", szTitle, VER_HIGH, VER_LOW) ;
               MessageBox(hWnd, buf, "SC Add Account", MB_OK|MB_ICONINFORMATION) ;
               return 0 ;
            case IDM_EXIT:
               DestroyWindow(hWnd);
               return 0 ;
            case IDB_READ:
               //EnableWindow(hwndCreate, FALSE) ;
               KillTimer(hWnd, TIMER_ID) ;
               oldCursor = SetCursor(WaitCursor) ;
               CardState = ProcessCard(hWnd, &CardParams) ;
               // Log entry
               if (WriteOutFile(hLogFile, "%s,%s,%s,%s,%u,%u,%u,%s,%u\r\n", FormatDate(), h->getUUID(), &CardParams.file[0][1], state_txt[CardState], CardParams.CardAgencyID, CardParams.ProductType, CardParams.ProductDesig, FormatExpDate(CardParams.ExpDate), CardParams.AccStatus))
                  {
                  ErrorBox(hWnd, "Error writing log file") ;
                  //return CARD_STATE_ERROR ;
                  } ;
               SetCursor(oldCursor) ;
               InvalidateRect(hWnd, NULL, TRUE) ;
               return 0 ;
            } ;
         break ;

      case WM_SC_CARD_STATUS:
         CardState = (CARD_STATE)wParam ;
         if (CardState == CARD_STATE_PRESENT)
            {
            if (h->Open(devNum))
               {
               KillTimer(hWnd, TIMER_ID) ;
               ErrorBox(hWnd, NULL) ;
               CardState = CARD_STATE_ERROR ;
               //EnableWindow(hwndCreate, FALSE) ;
               }
            else if (h->GetCardType() == CARD_TYPE_DESFIRE)
               {
               CardState = CheckCard(hWnd, &CardParams) ;
               if (CardState == CARD_STATE_CREATE) SetTimer(hWnd, TIMER_ID, 5000, NULL) ;
               }
            else
               {
               KillTimer(hWnd, TIMER_ID) ;
               ErrorBox(hWnd, "Invalid card type") ;
               CardState = CARD_STATE_ERROR ;
               //EnableWindow(hwndCreate, FALSE) ;
               } ;
            }
         else
            {
            KillTimer(hWnd, TIMER_ID) ;
            h->Close() ;
            //EnableWindow(hwndCreate, FALSE) ;
            } ;
         InvalidateRect(hWnd, NULL, TRUE) ;
         return 0 ;

      case WM_TIMER:
         KillTimer(hWnd, TIMER_ID) ;
         PostMessage(hWnd, WM_COMMAND, IDB_READ, 0) ;
         return 0 ;

      case WM_CHAR:
         if (wParam == 0x1b)
            {
            //ShowWindow(hWnd, SW_NORMAL) ;
            if (ws)
               SetWindowPos(hWnd, HWND_TOP, wx, wy, ww, wh, SWP_SHOWWINDOW) ;
            else
               SetWindowPos(hWnd, HWND_TOP, 0, 0, GetSystemMetrics(SM_CXSCREEN), GetSystemMetrics(SM_CYSCREEN), SWP_SHOWWINDOW) ;
            ws = !ws ;
            } ;
         break ;

      case WM_SYSCOMMAND:
         switch (wParam & 0xfff0)
            {
            case SC_MAXIMIZE:
               SetWindowPos(hWnd, HWND_TOP, 0, 0, GetSystemMetrics(SM_CXSCREEN), GetSystemMetrics(SM_CYSCREEN), SWP_SHOWWINDOW) ;
               return 0 ;
            case SC_RESTORE:
               SetWindowPos(hWnd, HWND_TOP, wx, wy, ww, wh, SWP_SHOWWINDOW) ;
               return 0 ;
            case SC_MOVE:
               if (ws) return 0 ;
            } ;
         break ;

      case WM_WINDOWPOSCHANGED:
         return 0 ;

      case WM_PAINT:
         GetClientRect(hWnd, &rect) ;
         rect.top += 35 ;
         rect.bottom -= 35 ;
         rect.left += 20 ;
         rect.right -= 20 ;
         x = (rect.left+rect.right) >> 1 ;
         y = ((rect.bottom - rect.top) >> 1) - 30 ;
		   hdc = BeginPaint(hWnd, &ps) ;
         SetTextAlign(hdc, TA_TOP|TA_CENTER) ;
         SetBkColor(hdc, GetSysColor(COLOR_BTNFACE)) ;
         SetBkMode(hdc, OPAQUE) ;
         DrawEdge(hdc, &rect, EDGE_ETCHED, BF_RECT) ;
         //SelectObject(hdc, GetStockObject(DEFAULT_GUI_FONT)) ;
         //wmId = sprintf_s(buf, sizeof(buf), "Agency: %u, Product: %u, Designator: %u, Active: %u", CardParams.AgencyID, CardParams.ProductType, CardParams.ProductDesig, CardParams.AccStatus) ;
         //TextOut(hdc, x, 12, buf, wmId) ;
         SelectObject(hdc, hTextFont) ;
         switch (CardState)
            {
            case CARD_STATE_EMPTY:
               TextOut(hdc, x, y, "Ready", 5) ;
               TextOut(hdc, x, y+45, "Place card on reader", 20) ;
               break ;
            case CARD_STATE_SUCCESS:
               SetTextColor(hdc, RGB(0, 127, 0)) ;
               TextOut(hdc, x, y, "Card Programmed", 15) ;
               TextOut(hdc, x, y+45, "Remove Card", 11) ;
               break ;
            case CARD_STATE_ALREADY:
               TextOut(hdc, x, y, "Card is already programmed", 26) ;
               TextOut(hdc, x, y+45, "Remove Card", 11) ;
               break ;
            case CARD_STATE_CREATE:
               SetTextColor(hdc, RGB(200, 0, 0)) ;
               TextOut(hdc, x, y, "Please wait...", 14) ;
               TextOut(hdc, x, y+45, "Do not remove card", 18) ;
               break ;
            case CARD_STATE_NO:
               break ;
            default:
               TextOut(hdc, x, y, "Error", 5) ;
               TextOut(hdc, x, y+45, "Remove Card", 11) ;
               break ;
            } ;
         EndPaint(hWnd, &ps);
         return 0 ;

      case WM_DESTROY:
         KillTimer(hWnd, TIMER_ID) ;
         StopCommThread() ;
         PostQuitMessage(0);
         return 0 ;
      } ;
	return DefWindowProc(hWnd, message, wParam, lParam) ;
   }
