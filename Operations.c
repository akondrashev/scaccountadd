#include "stdafx.h"


int ReadGFI_PIDFile(HARDWARE *h, CARD_PARAMS *lpParams)
   {
   DesfireFileDef *pFile ;

   // Select Application
	if (DES_selectApplication(h, GFI_APPLICATION)) return 1 ;
   // Pointer in database for Printed ID file for this application
   pFile = FindGFIFile(GFI_APPLICATION, GFI_FILE_PID) ;
   if (!pFile || pFile->access_read != AR_FREE || pFile->fileSize != 32) return 2 ;
   // Get AgencyID from PID file
   if (DES_readData(h, pFile->fileNum, 0, 32, lpParams->file[0])) return 3 ;
   if (crc16(lpParams->file[0], 32)) return 4 ;
   lpParams->CardAgencyID = swabw(*(WORD*)&lpParams->file[0][28]) ;
   return 0 ;
   }


BOOLEAN AuthenticatePICC(HARDWARE *h, iAgency AgencyID, iAgency DefaultAgency, KEY *pPICC_Key)
   {
   SetKeyDataAES(pPICC_Key, DES_KEY_MASTER, AgencyID, UNKNOWN_APPLICATION) ;
   if (2 == DES_Authenticate(h, pPICC_Key)) return TRUE ;
   
   if (DefaultAgency != AgencyID)
      {
      SetKeyDataAES(pPICC_Key, DES_KEY_MASTER, DefaultAgency, UNKNOWN_APPLICATION) ;
      if (2 == DES_Authenticate(h, pPICC_Key)) return TRUE ;
      } ;
   
   SetKeyDataDES(pPICC_Key, DES_KEY_0, 0, PICC_APPLICATION) ;
   if (2 == DES_Authenticate(h, pPICC_Key)) return TRUE ;

   SetKeyDataAES(pPICC_Key, DES_KEY_0, 0, UNKNOWN_APPLICATION) ;
   if (2 == DES_Authenticate(h, pPICC_Key)) return TRUE ;

   return FALSE ;
   }


BOOLEAN createGFIFiles(HWND hwndParent, HARDWARE *h, iAgency AgencyID, Application App)
   {
	KEY  AgencyKey, FileKey ;
   VECTOR_BYTE file_list ;
   size_t  i ;
   BYTE    fileID ;
   DesfireFileDef *pFile ;
   DES_KEY dk ;
   BYTE    buf[128] ;

   // Autheticate with application master key
   SetKeyDataAES(&AgencyKey, DES_KEY_0, AgencyID, App) ;
 	if (2 != DES_Authenticate(h, &AgencyKey))
      {
      ErrorBox(hwndParent, "Error authenticating application 0x%06X with master key", App) ;
		return TRUE ;
      } ;

   if (0 == getFileIDsByApp(&file_list, App))
      {
      ErrorBox(hwndParent, "No files defined for application 0x%06X", App) ;
      return FALSE ;
      } ;

   DES_setCommit(FALSE) ;

   // Create files
   for (i = 0; i < file_list.numElements; i++)
      {
      fileID = file_list.vector[i] ;
      switch (getFileType(fileID, App))
         {
	      case MDFT_STANDARD_DATA_FILE:
            if (DES_createGFIStandardFile(h, fileID, App))
               {
               ErrorBox(hwndParent, "Error creating Standard File %u for application 0x%06X", fileID, App) ;
				   return TRUE ;
				   } ;
				break;

		   case MDFT_BACKUP_DATA_FILE:
			   if (DES_createGFIBackupFile(h, fileID, App))
               {
               ErrorBox(hwndParent, "Error creating Backup File %u for application 0x%06X", fileID, App) ;
				   return TRUE ;
  				   } ;
            break;

		   case MDFT_CYCLIC_FILE_WITH_BACKUP:
			   if (DES_createGFICyclicFile(h, fileID, App))
               {
               ErrorBox(hwndParent, "Error creating Cyclic Record File %u for application 0x%06X", fileID, App) ;
				   return TRUE ;
  				   } ;
            dk = (DES_KEY)getFileAuthenticationKey(fileID, RW, App) ;
            SetKeyDataAES(&FileKey, dk, AgencyID, App) ;
		      if (2 != DES_Authenticate(h, &FileKey))
               {
               ErrorBox(hwndParent, "Error while authenticating file %u", fileID) ;
               return TRUE ;
               } ;
            pFile = FindApplicationFile(fileID, App) ;
            memset(buf, 0, pFile->fileSize) ;
            if (DES_writeRecord(h, fileID, buf, pFile->fileSize))
               {
               ErrorBox(hwndParent, "Error writing Cyclic Record File %u for application 0x%06X", fileID, App) ;
				   return TRUE ;
               } ;
            if (2 != DES_Authenticate(h, &AgencyKey))
               {
               ErrorBox(hwndParent, "Error authenticating application 0x%06X with master key", App) ;
		         return TRUE ;
               } ;
            DES_setCommit(TRUE) ;
            break;

		   case MDFT_VALUE_FILE_WITH_BACKUP:
			   if (DES_createGFIValueFile(h, fileID, App))
               {
               ErrorBox(hwndParent, "Error creating Value File %u for application 0x%06X", fileID, App) ;
				   return TRUE ;
  				   } ;
			   break;

		   case MDFT_LINEAR_FILE_WITH_BACKUP:
			   ErrorBox(hwndParent, "Create Linear Record %u file is not supported") ;
			   break;

         default:
            ErrorBox(hwndParent, "Unknown file type %u for file %u", getFileType(fileID, App), fileID) ;
	      } ;
      } ;

   if (DES_commit(h))
      {
      ErrorBox(hwndParent, "Error commiting files for application 0x%06X", App) ;
      return TRUE ;
      } ;
   return FALSE ;
   }


BOOLEAN writeGFIFiles(HWND hwndParent, HARDWARE *h, CARD_PARAMS *lpParams)
   {
   VECTOR_BYTE     file_list ;
   size_t   i ;
   BYTE     fileID ;
   DES_KEY  dk ;
   DES_KEY  last_dk = NOT_AUTHENTICATED ;
   KEY      Key ;

   if (DES_selectApplication(h, PICC_APPLICATION))
      {
      ErrorBox(hwndParent, "Error selecting PICC") ;
      return FALSE ;
      } ;

  	// Select Application
   if (DES_selectApplication(h, GFI_APP_ACCOUNT))
      {
      ErrorBox(hwndParent, "Error selecting application") ;
      return FALSE ;
      } ;

   // Get list of defined files
   if (0 == getFileIDsByApp(&file_list, GFI_APP_ACCOUNT))
      {
      ErrorBox(hwndParent, "No files defined for application") ;
      return FALSE ;
      } ;
      
   DES_setCommit(TRUE) ;

   // Loop by files list
   for (i = 0; i < file_list.numElements; i++)
	   {
      fileID = file_list.vector[i] ;
      // Authenticate, if needed
	   dk = (DES_KEY)getFileAuthenticationKey(fileID, RW, GFI_APP_ACCOUNT) ;
		if (last_dk != dk)
         {
         SetKeyDataAES(&Key, dk, lpParams->AgencyID, GFI_APP_ACCOUNT) ;
		   if (2 != DES_Authenticate(h, &Key))
            {
            ErrorBox(hwndParent, "Error while authenticating file %u", fileID) ;
            return FALSE ;
            } ;
		   last_dk = dk;
		   } ;

      if (DES_writeData(h, fileID, lpParams->file[i], 32))
         {
         ErrorBox(hwndParent, "Error writing file %u", fileID) ;
         return FALSE ;
         } ;

      } ;

   if (DES_commit(h))
      {
      ErrorBox(hwndParent, "Error commiting files") ;
      return false ;
      } ;
   return TRUE ;
   }


PROCESS_STATE CheckCard(HWND hWnd, CARD_PARAMS *lpParams)
   {
   VECTOR_DWORD app_list ;
   DesfireFileDef *pFile ;
   KEY      NewKey ;
   BYTE     buf[32] ;
   size_t   k ;

   // Get list of applications
   if (DES_getApplicationIDs(h, &app_list))
      {
      ErrorBox(hWnd, "Error getting application IDs") ;
      return CARD_STATE_ERROR ;
      } ;

   if (app_list.numElements == 0)
      {
      ErrorBox(hWnd, "No applications found") ;
      return CARD_STATE_ERROR ;
      } ;

   if (ReadGFI_PIDFile(h, lpParams))
      {
      ErrorBox(hWnd, "Error reading PID file") ;
      return CARD_STATE_ERROR ;
      } ;

   if (lpParams->AgencyID != lpParams->CardAgencyID)
      {
      ErrorBox(hWnd, "Invalid Agency") ;
      return CARD_STATE_ERROR ;
      } ;

   // Pointer in database for Card Properties file
   pFile = FindGFIFile(GFI_APPLICATION, GFI_FILE_CARD_PROP) ;
   if (!pFile || pFile->fileSize != 32)
      {
      ErrorBox(hWnd, "Invalid Card Properties file") ;
      return CARD_STATE_ERROR ;
      } ;

   SetKeyDataAES(&NewKey, (DES_KEY)pFile->access_rw, lpParams->CardAgencyID, GFI_APPLICATION) ;
   if (2 != DES_Authenticate(h, &NewKey))
      {
      ErrorBox(hWnd, "Authentication for Card Properties file") ;
      return CARD_STATE_ERROR ;
      } ;

   if (DES_readData(h, pFile->fileNum, 0, pFile->fileSize, buf))
      {
      ErrorBox(hWnd, "Error reading Card Properties") ;
      return CARD_STATE_ERROR ;
      } ;

   if (crc16(buf, 32))
      {
      ErrorBox(hWnd, "Card Properties file is corrupt") ;
      return CARD_STATE_ERROR ;
      } ;

   if (bfx(buf, 1, 8) == 0)
      {
      ErrorBox(hWnd, "Card Properties file is invalid") ;
      return CARD_STATE_ERROR ;
      } ;
   lpParams->UserProfile = bfx(buf, 9, 8) ;
   lpParams->ExpDate = bfx(buf, 17, 16) ;

   for (k = 0; k < app_list.numElements; k++)
      {
      if (app_list.vector[k] == GFI_APP_ACCOUNT) return CARD_STATE_ALREADY ;
      } ;

   return CARD_STATE_CREATE ;
   }


PROCESS_STATE  ProcessCard(HWND hWnd, CARD_PARAMS *lpParams)
   {
//   VECTOR_DWORD app_list ;
//   DesfireFileDef *pFile ;
   KEY      NewKey, PICC_Key, ZeroKey ;
//   BYTE     buf[32] ;
   size_t   k ;

#if 0
   // Get list of applications
   if (DES_getApplicationIDs(h, &app_list))
      {
      ErrorBox(hWnd, "Error getting application IDs") ;
      return CARD_STATE_ERROR ;
      } ;

   if (app_list.numElements == 0)
      {
      ErrorBox(hWnd, "No applications found") ;
      return CARD_STATE_ERROR ;
      } ;

   if (ReadGFI_PIDFile(h, lpParams))
      {
      ErrorBox(hWnd, "Error reading PID file") ;
      return CARD_STATE_ERROR ;
      } ;

   if (lpParams->AgencyID != lpParams->CardAgencyID)
      {
      ErrorBox(hWnd, "Invalid Agency") ;
      return CARD_STATE_ERROR ;
      } ;

   // Pointer in database for Card Properties file
   pFile = FindGFIFile(GFI_APPLICATION, GFI_FILE_CARD_PROP) ;
   if (!pFile || pFile->fileSize != 32)
      {
      ErrorBox(hWnd, "Invalid Card Properties file") ;
      return CARD_STATE_ERROR ;
      } ;

   SetKeyDataAES(&NewKey, (DES_KEY)pFile->access_rw, lpParams->CardAgencyID, GFI_APPLICATION) ;
   if (2 != DES_Authenticate(h, &NewKey))
      {
      ErrorBox(hWnd, "Authentication for Card Properties file") ;
      return CARD_STATE_ERROR ;
      } ;

   if (DES_readData(h, pFile->fileNum, 0, pFile->fileSize, buf))
      {
      ErrorBox(hWnd, "Error reading Card Properties") ;
      return CARD_STATE_ERROR ;
      } ;

   if (crc16(buf, 32))
      {
      ErrorBox(hWnd, "Card Properties file is corrupt") ;
      return CARD_STATE_ERROR ;
      } ;

   if (bfx(buf, 1, 8) == 0)
      {
      ErrorBox(hWnd, "Card Properties file is invalid") ;
      return CARD_STATE_ERROR ;
      } ;
   lpParams->UserProfile = bfx(buf, 9, 8) ;
   lpParams->ExpDate = bfx(buf, 17, 16) ;

   for (k = 0; k < app_list.numElements; k++)
      {
      if (app_list.vector[k] == GFI_APP_ACCOUNT) return CARD_STATE_ALREADY ;
      } ;
#endif

   // Select PICC
   if (DES_selectApplication(h, PICC_APPLICATION))
      {
      ErrorBox(hWnd, "Error selecting PICC Application") ;
	   return CARD_STATE_ERROR ;
      } ;
   // Load ZERO key
   SetKeyDataAES(&ZeroKey, DES_KEY_0, 0, UNKNOWN_APPLICATION) ;
   // Load master key
   SetKeyDataAES(&PICC_Key, DES_KEY_MASTER, lpParams->AgencyID, UNKNOWN_APPLICATION) ;
   // Authenticate PICC
   if (2 != DES_Authenticate(h, &PICC_Key))
      {
      ErrorBox(hWnd, "Error authenticating PICC with master key") ;
      return CARD_STATE_ERROR ;
      } ;
   // Create application
   if (DES_createApplication(h, GFI_APP_ACCOUNT, 14, KEY_AES))
      {
      ErrorBox(hWnd, "Error creating application") ;
	   return CARD_STATE_ERROR ;
      } ;
   // Select it
   if (DES_selectApplication(h, GFI_APP_ACCOUNT))
      {
      ErrorBox(hWnd, "Error selecting application") ;
      return CARD_STATE_ERROR ;
      } ;

   // Create account data file
   memset(lpParams->file[1], 0, 32) ;
   bfi(lpParams->file[1],   1,  8, 1) ;
   bfi(lpParams->file[1],   9,  1, lpParams->AccStatus) ;
   bfi(lpParams->file[1],  25,  8, lpParams->ProductType) ;
   bfi(lpParams->file[1],  33,  8, lpParams->ProductDesig) ;
   bfi(lpParams->file[1],  41,  8, lpParams->UserProfile) ;
   bfi(lpParams->file[1], 225, 16, lpParams->ExpDate) ;
   *(WORD*)&lpParams->file[1][30] = crc16(lpParams->file[1], 30) ;

   memset(lpParams->file[2], 0, 32) ;

   // Load keys
   for (k = 1; k <= 13; k++)
      {
      // Authenticate
      if (2 != DES_Authenticate(h, &ZeroKey))
         {
         ErrorBox(hWnd, "Error authenticating application") ;
         return CARD_STATE_ERROR ;
         } ;
      SetKeyDataAES(&NewKey, (DES_KEY)k, lpParams->AgencyID, GFI_APP_ACCOUNT) ;
      if (DES_changeKey(h, &NewKey, &ZeroKey, FALSE))
         {
         ErrorBox(hWnd, "Error changing key %u for application", k) ;
         return CARD_STATE_ERROR ;
         } ;
      } ;
   // Change application master key
   if (2 != DES_Authenticate(h, &ZeroKey))
      {
      ErrorBox(hWnd, "Error authenticating application") ;
      return CARD_STATE_ERROR ;
      } ;
   SetKeyDataAES(&NewKey, DES_KEY_0, lpParams->AgencyID, GFI_APP_ACCOUNT) ;
   if (DES_changeKey(h, &NewKey, &ZeroKey, TRUE))
      {
      ErrorBox(hWnd, "Error changing master key for application") ;
      return CARD_STATE_ERROR ;
      } ;
   // Create files
   if (createGFIFiles(hWnd, h, lpParams->AgencyID, GFI_APP_ACCOUNT)) return CARD_STATE_ERROR ;
   if (!writeGFIFiles(hWnd, h, lpParams)) return CARD_STATE_ERROR ;

   return CARD_STATE_SUCCESS ;
   }
