#ifndef _agencykeys_h_
#define _agencykeys_h_

typedef WORD iAgency ;

// Card types, as reported by HID
#define  CARD_TYPE_CLASSIC       0x43
#define  CARD_TYPE_ULTRALIGHT    0x44
#define  CARD_TYPE_DESFIRE       0x45
#define  CARD_TYPE_ULTRALIGHT_C  0x50
#define  CARD_TYPE_NANO          0x51

#define  CARD_TYPE_TRIM          0x10

// CLASSIC authentication
#define MC_Sector                0x01
extern const uint8_t CLASSIC_KEYS_A[8] ;
extern const uint8_t CLASSIC_KEYS_B[8] ;

typedef enum
   {
	PICC_APPLICATION =	   0x00000000,
	GFI_APPLICATION =		   0x00F21201,
   GFI_APP_ACCOUNT =		   0x00F21202,
	GFI_APP_TUCSON =		   0x00F21200,
	GFI_PID_TUCSON =	      0x00F211F0,
	GFI_PURSE_TUCSON =	   0x00F21210,
   APP_CLASSIC =           0x10000000,
   APP_ULTRALIGHT =        0x20000000,
   APP_ULTRALIGHT_C =      0x40000000,
	UNKNOWN_APPLICATION =	0xFFFFFFFF
	} Application;


typedef struct
   {
	unsigned char KeyVersion;
	unsigned char Key[16];
   } T_AES_KEY ;

typedef struct
   {
   unsigned char KeyVersion;
	char Key[32];
   } T_CVV_KEY ;

typedef struct
   {
	T_AES_KEY AesPICCKey;
	T_AES_KEY AesApplicationKey[14];
   T_AES_KEY ULCKey;
   T_CVV_KEY CVVKey ;
   } DESFIRE_AES_KEYS ;


typedef struct
   {
	const iAgency           AgencyID ;
   const Application       AppID ;
	const DESFIRE_AES_KEYS *Keys;
	const char              AgencyFullName[80];
   } GFI_AGENCY ;


#define  AGENCY_AKRON           48           /* Akron, Oh                  */
#define  AGENCY_ALBANY         194           /* Albany, NY                 */
#define  AGENCY_ANAHEIM        288           /* Anaheim, CA                */
#define  AGENCY_ANNAPOLIS      375           /* Annapolis, MD              */
#define  AGENCY_ANNARBOR       145           /* Ann Arbor, MI              */
#define  AGENCY_ASPEN          302           /* Aspen, CO (0610)           */
#define  AGENCY_ATHENS         180           /* Athens GA (1114)           */
#define  AGENCY_AUSTIN         200           /* Austin,TX                  */
#define  AGENCY_BELLINGHAM     271           /* Bellingham, WA (4824)      */
#define  AGENCY_BLOOMINGTON_IL 382           /* Bloomington, IL (1436)     */
#define  AGENCY_BOISE           90           /* Boise, ID                  */
#define  AGENCY_BURLINGTON_NC  380           /* Burlington, NC             */
#define  AGENCY_BURLINGTON      30           /* Burlington Vt              */
#define  AGENCY_BUFFALO        119           /* Buffalo, NY                */
#define  AGENCY_CHARLESTON_SC   58           /* Charleston, SC (4107)      */
#define  AGENCY_CHARLOTTE       54           /* Charlotte, NC (3403)       */
#define  AGENCY_CHICAGO        303           /* Chicago, IL                */
#define  AGENCY_CINNCINATI     204           /* Cinncinati, OH             */
#define  AGENCY_CDOT           371           /* Golden, CO                 */
#define  AGENCY_CLOVIS         377           /* Clovis, CA                 */
#define  AGENCY_COTA             5           /* Columbus, OH - COTA (3606) */
#define  AGENCY_CT               4           /* Hartford/misc, CT          */
#define  AGENCY_DETROIT_SMART    8           /* Detroit, MI (2318)         */
#define  AGENCY_DULUTH         235           /* Duluth, Minnesota          */
#define  AGENCY_ELMIRA_NY       61           /* Elmira, NY (3309)          */
#define  AGENCY_ERIE           199           /* Erie, PA                   */
#define  AGENCY_EVANSVILLE      36           /* Evansville, IN (1502)      */
#define  AGENCY_FAIRFIELD       98           /* Fairfield, OH              */
#define  AGENCY_FORT_WORTH     167           /* Fort Worth, TX (0167)      */
#define  AGENCY_FORT_WRIGHT     32           /* Fort Wright, KY (1804)     */
#define  AGENCY_FRESNO         360           /* Fresno, CA (0506)          */
#define  AGENCY_CLEVELAND       37           /* Cleveland, OH - GCRTA      */
#define  AGENCY_GOLDEN         371           /* Golden, CO                 */
#define  AGENCY_GREELY         367           /* Greely, CO                 */
#define  AGENCY_GREENFIELD     449           /* Greenfield, MA (2210)      */
#define  AGENCY_GREENSBORO     108           /* Greensboro, NC             */
#define  AGENCY_HAMPTON        101           /* Hampton, VA - (0101)       */
#define  AGENCY_HIGHPOINT      207           /* Highpoint, NC (3439)       */
#define  AGENCY_JACKSONVILLE   414           /* Jacksonville, FL - JTA     */
#define  AGENCY_LAFAYETTE        9           /* Lafayette, IN              */
#define  AGENCY_LANSING        175           /* Lansing MI, CATA - (2311)  */
#define  AGENCY_LAS_VEGAS       12           /* Las Vegas, NV ( 2906 )     */
#define  AGENCY_LIMO             6           /* LIMOCAR Canada             */
#define  AGENCY_LITTLEROCK      23           /* Little Rock, AR            */
#define  AGENCY_LOMPOC         494           /* Lompoc, CA (0597)          */
#define  AGENCY_LOUISVILLE_KY  388           /* Louisville, KY (1802)      */
#define  AGENCY_LYNX           158           /* Orlando, Lynx              */
#define  AGENCY_MADISON        219           /* Madison WI.                */
#define  AGENCY_MANCHESTER_NH  197           /* Manachester, NH (3001)     */
#define  AGENCY_MANKATO_MN     440           /* Mankato, MN                */
#define  AGENCY_MARKHAM         24           /* Markham Can.               */
#define  AGENCY_MISSOULA       157           /* Missoula, MT               */
#define  AGENCY_MONROE         364           /* Monroe, MI                 */
#define  AGENCY_MINNEAPOLIS_MN   2           /* Minneapolis, MN (2406)     */
#define  AGENCY_NASHVILLE      185           /* Nashville, TN              */
#define  AGENCY_NEW_ORLEANS    188           /* New Orleans, LA            */
#define  AGENCY_OAKLAND         96           /* Oakland, CA (AC Transit)   */
#define  AGENCY_ONEBUS          14           /* Orange/Newark/Elizabeth, NJ*/
#define  AGENCY_ORANGE_CO      141           /* Garden Grove, Anaheim, (0508) */
#define  AGENCY_OWENSOUND       82           /* Owen Sound - CANADA        */
#define  AGENCY_PHILADELPHIA   337           /* Philadelphia, PA (SEPTA)   */
#define  AGENCY_PINOLE         292           /* Pinole, CA                 */
#define  AGENCY_PITTSBURGH_PA   68           /* Pittsburgh, PA             */
#define  AGENCY_PORTERVILLE    482           /* Porterville, CA ( 0557 )   */
#define  AGENCY_PUGENT_SOUND   384           /* Seattle, WA - Pugent (4832)*/
#define  AGENCY_RALEIGH        161           /* Raleigh, NC                */
#define  AGENCY_READING        164           /* Reading, PA                */
#define  AGENCY_RICHMOND        76           /* Richmond, VA               */
#define  AGENCY_RIVERSIDE      165           /* Riverside CA               */
#define  AGENCY_ROCHESTER_NY   107           /* Rochester, NY              */
#define  AGENCY_ROCHESTER_PA    51           /* Rochester, PA              */
#define  AGENCY_SACRAMENTO     179           /* Sacramento, CA             */
#define  AGENCY_SAN_FRANCISCO  492           /* San Francisco, CA          */
#define  AGENCY_SANTA_BARBARA  117           /* Santa Barbara, CA (0117)   */
#define  AGENCY_SANTA_MARIA    308           /* Santa Maria, CA (0515)     */
#define  AGENCY_SANTA_MONICA    60           /* Santa Monica, CA           */
#define  AGENCY_SEATTLE          1           /* Seattle                    */
#define  AGENCY_SPOKANE        273           /* Spokane,WA                 */
#define  AGENCY_SPRINGFIELD     63           /* Springfield, MA ( 2213 )   */
#define  AGENCY_ST_CLOUD       106           /* St. Cloud, MN              */
#define  AGENCY_ST_JOHNS        84           /* St. Johns, NF              */
#define  AGENCY_STATECOLLEGE    27           /* State College, Pa          */
#define  AGENCY_STILLWATER     190           /* Stillwater OK              */
#define  AGENCY_SYRACUSE        13           /* Sycracuse, NY (3326)       */
#define  AGENCY_TACOMA         362           /* Tacoma, WA (Pierce Trans)  */
#define  AGENCY_TEMPE           52           /* Tempe, Az                  */
#define  AGENCY_TERREBONE      147           /* Terrebone, Quebec, Canada  */
#define  AGENCY_TEXAS_CITY     368           /* Texas City, TX             */
#define  AGENCY_TORONTO         59           /* Toronto (Downsview)        */
#define  AGENCY_TUCSON           7           /* Tucson, AZ                 */
#define  AGENCY_TURLOCK        379           /* Turlock, CA                */
#define  AGENCY_UTICA          246           /* Utica, NY (3326)           */
#define  AGENCY_VICTORIA       127           /* Victoria, BC, Canada       */
#define  AGENCY_WESTCHESTER     15           /* Westchester County         */
#define  AGENCY_WEST_PALM_BEACH 79           /* West Palm Beach            */
#define  AGENCY_WHISTLER       118           /* Whistler, BC, Canada       */
#define  AGENCY_WINNIPEG       350           /* Winnipeg, Canada           */
#define  AGENCY_COCOA          133           /* Winter Garden, Fl          */
#define  AGENCY_WORCESTER       39           /* Worcester, MA              */
#define  AGENCY_YOLO            50           /* Woodland, Ca               */
#define  AGENCY_YAPHANK         87           /* Yaphank, NY (0087)         */
#define  AGENCY_PHOENIX       1555           /* Phoenix, Az                */
#define  AGENCY_MARKET        4094           /* marketing                  */
#define  AGENCY_MARKETING     4094
#define  AGENCY_GFI           4095           /* GFI GENFARE                */

extern GFI_AGENCY Agencies[] ;

typedef enum
   {
   DES_KEY_0 = 0,
	DES_KEY_1,
	DES_KEY_2,
	DES_KEY_3,
	DES_KEY_4,
	DES_KEY_5,
	DES_KEY_6,
	DES_KEY_7,
	DES_KEY_8,
	DES_KEY_9,
	DES_KEY_10,
	DES_KEY_11,
	DES_KEY_12,
	DES_KEY_13,
   DES_KEY_FREE,
   DES_KEY_DENY,
   NOT_AUTHENTICATED = 0x1F,
   DES_KEY_MASTER = 0xFF
   } DES_KEY ;


typedef enum
   {
	KEY_DES  = 0x00,
	KEY_3DES = 0x40,
	KEY_AES  = 0x80,
	} CryptoMode ;


typedef enum
   {
	R = 0,
	W,
	RW,
	C
	} AccessMode ;


typedef enum
   {
	KEY_ENCIPHER = 0,
   KEY_DECIPHER = 1
   } DESFireCipher ;


typedef enum
   {
   CBC_SEND,
   CBC_RECEIVE
   } DESFireCBC ;


#define DES_LONG DWORD
typedef unsigned char DES_cblock[8] ;


typedef struct
   {
   union
      {
      DES_cblock cblock ;
      DES_LONG deslong[2] ;
      } ks[16] ;
   } DES_key_schedule ;



typedef struct
   {
   T_AES_KEY  key ;
   DES_KEY    keyNum ;
   CryptoMode keyType ;
   BYTE       IV[16] ;
   size_t     BlockSize ;
   size_t     KeySize ;
   DES_key_schedule mk_ks1; // first  component of a TDEA key
   DES_key_schedule mk_ks2; // second component of a TDEA key
   DES_key_schedule mk_ks3; // third  component of a TDEA key
   } KEY ;


// DESFire application files types
typedef enum
	{
	MDFT_STANDARD_DATA_FILE = (BYTE)0x00,
	MDFT_BACKUP_DATA_FILE = (BYTE)0x01,				
	MDFT_VALUE_FILE_WITH_BACKUP = (BYTE)0x02,			
	MDFT_LINEAR_FILE_WITH_BACKUP = (BYTE)0x03, 
	MDFT_CYCLIC_FILE_WITH_BACKUP = (BYTE)0x04,
   MDFT_INVALID_FIE_TYPE = (BYTE)0xFF
	}  DESFireFileType ;


typedef enum
   {
   GFI_FILE_PID         = (BYTE)0,
   GFI_FILE_CARD_PROP   = (BYTE)1,
   GFI_FILE_PROD_LIST   = (BYTE)2,
   GFI_FILE_PROD        = (BYTE)3,
   GFI_FILE_TRANSFER    = (BYTE)4,
   GFI_FILE_PAYGO       = (BYTE)5,
   GFI_FILE_BONUS       = (BYTE)6,
   GFI_FILE_PURSE       = (BYTE)7,
   GFI_FILE_PURSE_LOAD  = (BYTE)8,
   GFI_FILE_ADD_FTRS    = (BYTE)9,
   GFI_FILE_ACCOUNT     = (BYTE)10,
   GFI_FILE_LOG         = (BYTE)11,
   GFI_FILE_SPARE       = (BYTE)12
   } GFIFileType ;


// Defines if data transmitted to files is encrypted (with the session key) or secured with a MAC
typedef enum
	{
	CM_PLAIN = 0x00,
	CM_MAC = 0x01,			
	CM_ENCRYPT = 0x03,
	} DESFireFileEncryption ;


typedef enum
	{
	AR_KEY0 =  (unsigned char)0x00, 
	AR_KEY1 =  (unsigned char)0x01, 
	AR_KEY2 =  (unsigned char)0x02, 
	AR_KEY3 =  (unsigned char)0x03,
	AR_KEY4 =  (unsigned char)0x04,
	AR_KEY5 =  (unsigned char)0x05,
	AR_KEY6 =  (unsigned char)0x06,
	AR_KEY7 =  (unsigned char)0x07,
	AR_KEY8 =  (unsigned char)0x08,
	AR_KEY9 =  (unsigned char)0x09,
	AR_KEY10 = (unsigned char)0x0A,
	AR_KEY11 = (unsigned char)0x0B,
	AR_KEY12 = (unsigned char)0x0C,
	AR_KEY13 = (unsigned char)0x0D,
	AR_FREE =  (unsigned char)0x0E,		
	AR_NEVER = (unsigned char)0x0F		
	}  DESFireAccessRights ;


#define MAX_APP_FILE_NUM         22

#define FILE_BUF_MAX_LEN         64


#define TRANSFER_MG       0
#define PERIOD_MG         1
#define S_RIDE_MG         2
#define S_VALUE_MG        3
#define S_PURSE_MG        4
#define TOK_TICK_MG       5
#define ALT_KEY_MG        6
#define MAINT_EMP_MG      7
#define CHANGE_MG         8
#define RECEIPT_MG        9
#define CHARGE_CARD      10
#define SMART_CARD       11
#define SMART_VALUE_CARD 12
#define SMART_RIDE_CARD  13
#define PAYGO_DESIG      30      // PAYGO designator (constant)
#define BONUS_DESIG      31      // BONUS designator (constant)
#define BONUS_SLOT       11      // BONUS Autoloads use Slot=11 (ask the people who live in the Cloud)
#define NO_MEDIA_TYPE    255

typedef enum
   {
   GFI_MUST,
   GFI_OPT
   } GFIDisposition ;

typedef struct
   {
   unsigned char  IsDefined ;
   unsigned char  CalcCRC ;
   unsigned char  PIDPos ;                   // 0 - do not insert PID; (n-1) - PID position in file
   unsigned char  AgencyPos ;                // 0 - do not insert Agency ID; (n-1) - Agency ID position in file
   unsigned char  data[FILE_BUF_MAX_LEN] ;
   } FILE_BUF ;


typedef struct
   {
   Application             AppID ;
   unsigned char           fileNum ;
	DESFireFileType         FileType ;
   GFIFileType             GFIFileType ;
	GFIDisposition          Disposition ;
   DESFireAccessRights     access_read ;
   DESFireAccessRights     access_write ;
   DESFireAccessRights     access_rw ;
   DESFireAccessRights     access_change ;
   int                     fileSize ;              // File or record size / Min value for value file
   int                     maxRecords ;            // Max records for cyclic file / Max value for value file
   int                     Value ;
   unsigned char           LimitedCreditEnabled;
	unsigned char           FreeReadEnabled;
   FILE_BUF                FileBuf ;
   } DesfireFileDef ;


typedef struct
   {
   unsigned char           fileNum ;
	DESFireFileType         FileType ;
   DESFireAccessRights     access_read ;
   DESFireAccessRights     access_write ;
   DESFireAccessRights     access_rw ;
   DESFireAccessRights     access_change ;
   int                     fileSize ;              // File or record size / Min value for value file
   int                     maxRecords ;            // Max records for cyclic file / Max value for value file
   int                     curRecords ;
   } FILE_DESCR ;

extern DesfireFileDef AppFilesList[] ;

#define SC_MAX_FILES       32
#define SC_MAX_RECORDS     10

// Value file data
typedef struct
   {
   int      Value ;
   int      Min ;
   int      Max ;
   } VALUE ;

// File read buffer
typedef union
   {
   BYTE         bytes[FILE_BUF_MAX_LEN] ;
   VALUE        ValueData ;
   } FILE_DATA ;

// Decoded data
typedef union
   {
   PRINTED_ID_T     PrintedID ;
   DES_CARD_PROP    CardProp ;
   DES_PRODUCT_LIST ProdList ;
   DES_TRANS        Transfer ;
   DES_PROD_FILE    Product ;
   DES_JOURNAL      Journal ;
   DES_ACCOUNT      Account ;
   DES_FILE_SPARE   Spare ;
   } DECODED_DATA ;


// File read buffer record
typedef struct
   {
   HWND              hwndControl ;        // Handle of control window that displays data
   BOOLEAN           IsDefined ;          // TRUE, if file exists
   BOOLEAN           IsCRC_OK ;           // TRUE id file CRC is valid
   BOOLEAN           IsUpdated ;          // TRUE, if data changed
   BYTE              numRecords ;         // Number of records read from log file
   BYTE              currRecord ;
   DesfireFileDef   *pFile ;              // Pointer to files database
   FILE_DATA         raw_data ;           // File data
   DECODED_DATA      decoded ;
   } SC_FILE ;



size_t   GetAgencyTableSize(void) ;
size_t   AgencyIDToIndex(iAgency ID, Application App) ;
size_t   GetFileDescrTableSize(void) ;
size_t   GetZeroAgency(void) ;
size_t   getApplicationIDbyAgency(VECTOR_DWORD *lpApp, iAgency AgencyID) ;
size_t   getFileIDsByApp(VECTOR_BYTE *lpFileID, Application App) ;
void     ResetApplicationFiles(void) ;
BOOLEAN  IsAppDefinedForAgency(iAgency AgencyID, Application App) ;
const char* GetAgencyName(iAgency AgencyID) ;
DesfireFileDef* FindApplicationFile(unsigned char file, Application App) ;
DesfireFileDef* FindGFIFile(Application App, GFIFileType GFIFileType) ;


const char* DecodeApplication(Application App) ;

DESFireAccessRights  getFileAuthenticationKey(unsigned char file, AccessMode mode, Application App) ;
DESFireFileType getFileType(unsigned char file, Application App) ;
int      getFileSize(unsigned char file, Application App) ;
int      getRecordSize(unsigned char file, Application App) ;
void     SetKeyDataAES(KEY *pKey, DES_KEY num, iAgency AgencyID, Application App) ;
void     SetKeyDataDES(KEY *pKey, DES_KEY num, iAgency AgencyID, Application App) ;
const char* getCVVKey(iAgency AgencyID, Application App) ;
const unsigned char* getULCKey(iAgency AgencyID) ;

BOOLEAN IsKeyDefined(KEY *pKey) ;


#endif