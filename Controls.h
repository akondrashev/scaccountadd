#ifndef _CONTROLS_H_
#define _CONTROLS_H_

#define  MAIN_WND_WIDTH             400
#define  MAIN_WND_HEIGTH            300

// Top offset in main window
#define Y_WND_OFFSET                5
// Left offset in main window
#define X_WND_OFFSET                10

// Text spacing
#define V_SPACING                   20
// First line offset from control border
#define V_OFFSET                    20
// Left offset from control border
#define X_OFFSET                    10
// Left title offset from the control border corner
#define X_TITILE_OFFSET             7

// Edit control height
#define E_HEIGHT                    17

#define WM_SC_INIT                  WM_USER

#define AgencyWndX                  X_WND_OFFSET
#define AgencyWndY                  Y_WND_OFFSET
#define AgencyWndWidth              530
#define AgencyWndHeight             90
#define WM_SC_SELECTED_AGENCY    (WM_USER+3)
#define WM_SC_SET_AGENCY         (WM_USER+4)
HWND CreateAgencyWindow(HWND hwndParent, int xPos, int yPos) ;


#define CardStatusWndX              (AgencyWndX+AgencyWndWidth+10)
#define CardStatusWndY              Y_WND_OFFSET
#define CardStatustWndWidth         145
#define CardStatustWndHeight        90
#define WM_SC_CARD_STATUS      (WM_USER+1)
#define WM_SC_SELECTED_APP     (WM_USER+2)
HWND CreateCardStatusWindow(HWND hwndParent, int xPos, int yPos) ;



#define PrintedIDX                  X_WND_OFFSET
#define PrintedIDY                  (AgencyWndY+AgencyWndHeight+10)
#define PrintedIDWidth              150
#define PrintedIDHeight             110
#define WM_SC_GET_AGENCY
HWND CreatePrintedIDWindow(HWND hwndParent, int xPos, int yPos) ;


#define CardPropertiesX             X_WND_OFFSET
#define CardPropertiesY             (PrintedIDY+PrintedIDHeight+15)
#define CardPropertiesWidth         150
#define CardPropertiesHeight        430
HWND CreateCardPropertyWindow(HWND hwndParent, int xPos, int yPos) ;


#define ProdListX                   (CardPropertiesX+CardPropertiesWidth+10)
#define ProdListY                   PrintedIDY
#define ProdListWidth               185
#define ProdListHeight              555
HWND  CreateProdListWindow(HWND hwndParent, int xPos, int yPos) ;
DWORD GetGroupByProdID(DWORD product_id) ;


#define TransferX                   (ProductX+ProductWidth+10+ProductWidth+10)
#define TransferY                   Y_WND_OFFSET
#define TransferWidth               165
#define TransferHeight              330
HWND CreateTransferWindow(HWND hwndParent, int xPos, int yPos, GFIFileType TransferType) ;


//#define PayGoX                      (TransferX+TransferWidth+10)
//#define PayGoY                      Y_WND_OFFSET

#define PayGoX                      (TransferX+TransferWidth+10)
#define PayGoY                      Y_WND_OFFSET

#define BonusX                      (PayGoX+TransferWidth+10)
#define BonusY                      Y_WND_OFFSET

#define ProductX                    (ProdListX+ProdListWidth+10)
#define ProductY                    (ProdListY)
#define ProductWidth                160
#define ProductHeight               530
int  GetProductFileIndex(size_t prodNum) ;
HWND CreateProductWindow(HWND hwndParent, int xPos, int yPos, size_t prodNum) ;

#define JournalX                    TransferX
#define JournalY                    (TransferY+TransferHeight+90)
#define JournalWidth                185
#define JournalHeight               210
HWND CreateJournalWindow(HWND hwndParent, int xPos, int yPos, size_t recordNum) ;


#define AccountX                    (CardPropertiesX+CardPropertiesWidth+10)
#define AccountY                    PrintedIDY
#define AccountWidth                155
#define AccountHeight               210
HWND CreateAccountWindow(HWND hwndParent, int xPos, int yPos) ;


#define ULCardPropertiesX           X_WND_OFFSET
#define ULCardPropertiesY           (AgencyWndY+AgencyWndHeight+10)
#define ULCardPropertiesWidth       150
#define ULCardPropertiesHeight      370
HWND CreateULCardPropertyWindow(HWND hwndParent, int xPos, int yPos) ;

#define ULCardProductX              (ULCardPropertiesX+ULCardPropertiesWidth+10)
#define ULCardProductY              ULCardPropertiesY
#define ULCardProductWidth          175
#define ULCardProductHeight         370
#define ULCardProductX2             (ULCardProductX+ULCardProductWidth+10)
HWND CreateULCardProductWindow(HWND hwndParent, int xPos, int yPos, BYTE prodNum) ;

#define ULCardJournalX              (ULCardProductX2+ULCardProductWidth+10)
#define ULCardJournalY              ULCardPropertiesY
#define ULCardJournalWidth          195
#define ULCardJournalHeight         370
HWND CreateULCardJournalWindow(HWND hwndParent, int xPos, int yPos) ;


#define ULCPrintedIDX               X_WND_OFFSET
#define ULCPrintedIDY               (AgencyWndY+AgencyWndHeight+10)
#define ULCPrintedIDWidth           155
#define ULCPrintedIDHeight          130
HWND CreateULCPrintedIDWindow(HWND hwndParent, int xPos, int yPos) ;

#define ULCCardPropertiesX          (ULCPrintedIDX+ULCPrintedIDWidth+10)
#define ULCCardPropertiesY          ULCPrintedIDY
#define ULCCardPropertiesWidth      150
#define ULCCardPropertiesHeight     370
HWND CreateULCCardPropertyWindow(HWND hwndParent, int xPos, int yPos) ;

#define ULCCardProductX             (ULCCardPropertiesX+ULCCardPropertiesWidth+10)
#define ULCCardProductY             ULCCardPropertiesY
#define ULCCardProductWidth         175
#define ULCCardProductHeight        370
#define ULCCardProductX2            (ULCCardProductX+ULCCardProductWidth+10)
HWND CreateULCCardProductWindow(HWND hwndParent, int xPos, int yPos, BYTE prodNum) ;

#define ULCCardJournalX             (ULCCardProductX2+ULCCardProductWidth+10)
#define ULCCardJournalY             ULCCardPropertiesY
#define ULCCardJournalWidth         180
#define ULCCardJournalHeight        370
HWND CreateULCCardJournalWindow(HWND hwndParent, int xPos, int yPos) ;


#define ClassCardPropertiesX        X_WND_OFFSET
#define ClassCardPropertiesY        (AgencyWndY+AgencyWndHeight+10)
#define ClassCardPropertiesWidth    150
#define ClassCardPropertiesHeight   295
HWND CreateClassicCardPropertyWindow(HWND hwndParent, int xPos, int yPos) ;

#define MagneticCardPropertiesX        X_WND_OFFSET
#define MagneticCardPropertiesY        (AgencyWndY+AgencyWndHeight+10)
#define MagneticCardPropertiesWidth    360
#define MagneticCardPropertiesHeight   350
HWND CreateMagneticCardPropertyWindow(HWND hwndParent, int xPos, int yPos) ;

#define ClassicCardProductWidth     175
#define ClassicCardProductHeight    255
#define ClassicCardProductX         (ClassCardPropertiesX+ClassCardPropertiesWidth+10)
#define ClassicCardProductX2        (ClassicCardProductX+ClassicCardProductWidth+10)
#define ClassicCardProductY         ClassCardPropertiesY
HWND CreateClassicCardProductWindow(HWND hwndParent, int xPos, int yPos, BYTE prodNum) ;

HWND CreateMagneticCardProductWindow(HWND hwndParent, int xPos, int yPos, BYTE prodNum) ;


#define CreateEmptyCardWidth        290
#define CreateEmptyCardHeight       180

#define CreateCardWidth             290
#define CreateCardHeight            200

#define WM_SC_CHECK_FOR_UPDATE      (WM_USER+5)

void DestroyContols(void) ;
int  UpdateControls(HARDWARE *h) ;





#endif